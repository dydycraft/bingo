# Bingo

## Commands

For setup datapack :

```m
/function bingo:setup
```

For select random team :

```m
/function bingo:randomteam
```

For start the game :

```m
/function bingo:start
```

For reset :

```m
/function bingo:reset
```

## Configuration 

### Edit team number

Edit randomteam.mcfunction and comment the row to disable the team by prefixing the row with '#'.

### Edit number of point to win

Edit win.mcfunction and change the default value (102 is total number of minecraft advancements) and change to own value to win.
