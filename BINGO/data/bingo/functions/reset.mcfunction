team remove red
team remove blue
team remove green
team remove yellow
team remove black
team remove white
team remove aqua
team remove gray
team remove gold
team remove dark_purple
team remove dark_blue
team remove dark_green
team remove dark_aqua
team remove dark_red
team remove dark_gray
team remove light_purple

kill @e[type=armor_stand,name="red"]
kill @e[type=armor_stand,name="blue"]
kill @e[type=armor_stand,name="green"]
kill @e[type=armor_stand,name="yellow"]
kill @e[type=armor_stand,name="black"]
kill @e[type=armor_stand,name="white"]
kill @e[type=armor_stand,name="aqua"]
kill @e[type=armor_stand,name="gray"]
kill @e[type=armor_stand,name="gold"]
kill @e[type=armor_stand,name="dark_purple"]
kill @e[type=armor_stand,name="dark_blue"]
kill @e[type=armor_stand,name="dark_green"]
kill @e[type=armor_stand,name="dark_aqua"]
kill @e[type=armor_stand,name="dark_red"]
kill @e[type=armor_stand,name="dark_gray"]
kill @e[type=armor_stand,name="light_purple"]

advancement revoke @a everything

scoreboard objectives remove Scores
scoreboard objectives remove scoretmp