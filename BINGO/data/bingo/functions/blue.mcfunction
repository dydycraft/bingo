execute if entity @a[limit=1,team=blue,advancements={story/root=true}] run advancement grant @a[team=blue] only minecraft:story/root
execute if entity @a[limit=1,team=blue,advancements={story/root=true}] run tag @e[type=armor_stand,tag=!story-root-ok,name="blue"] add story-root
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-root

execute if entity @a[limit=1,team=blue,advancements={story/mine_stone=true}] run advancement grant @a[team=blue] only minecraft:story/mine_stone
execute if entity @a[limit=1,team=blue,advancements={story/mine_stone=true}] run tag @e[type=armor_stand,tag=!story-mine_stone-ok,name="blue"] add story-mine_stone
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-mine_stone-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-mine_stone

execute if entity @a[limit=1,team=blue,advancements={story/upgrade_tools=true}] run advancement grant @a[team=blue] only minecraft:story/upgrade_tools
execute if entity @a[limit=1,team=blue,advancements={story/upgrade_tools=true}] run tag @e[type=armor_stand,tag=!story-upgrade_tools-ok,name="blue"] add story-upgrade_tools
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-upgrade_tools-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-upgrade_tools

execute if entity @a[limit=1,team=blue,advancements={story/smelt_iron=true}] run advancement grant @a[team=blue] only minecraft:story/smelt_iron
execute if entity @a[limit=1,team=blue,advancements={story/smelt_iron=true}] run tag @e[type=armor_stand,tag=!story-smelt_iron-ok,name="blue"] add story-smelt_iron
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-smelt_iron-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-smelt_iron

execute if entity @a[limit=1,team=blue,advancements={story/obtain_armor=true}] run advancement grant @a[team=blue] only minecraft:story/obtain_armor
execute if entity @a[limit=1,team=blue,advancements={story/obtain_armor=true}] run tag @e[type=armor_stand,tag=!story-obtain_armor-ok,name="blue"] add story-obtain_armor
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-obtain_armor-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-obtain_armor

execute if entity @a[limit=1,team=blue,advancements={story/lava_bucket=true}] run advancement grant @a[team=blue] only minecraft:story/lava_bucket
execute if entity @a[limit=1,team=blue,advancements={story/lava_bucket=true}] run tag @e[type=armor_stand,tag=!story-lava_bucket-ok,name="blue"] add story-lava_bucket
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-lava_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-lava_bucket

execute if entity @a[limit=1,team=blue,advancements={story/iron_tools=true}] run advancement grant @a[team=blue] only minecraft:story/iron_tools
execute if entity @a[limit=1,team=blue,advancements={story/iron_tools=true}] run tag @e[type=armor_stand,tag=!story-iron_tools-ok,name="blue"] add story-iron_tools
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-iron_tools-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-iron_tools

execute if entity @a[limit=1,team=blue,advancements={story/deflect_arrow=true}] run advancement grant @a[team=blue] only minecraft:story/deflect_arrow
execute if entity @a[limit=1,team=blue,advancements={story/deflect_arrow=true}] run tag @e[type=armor_stand,tag=!story-deflect_arrow-ok,name="blue"] add story-deflect_arrow
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-deflect_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-deflect_arrow

execute if entity @a[limit=1,team=blue,advancements={story/form_obsidian=true}] run advancement grant @a[team=blue] only minecraft:story/form_obsidian
execute if entity @a[limit=1,team=blue,advancements={story/form_obsidian=true}] run tag @e[type=armor_stand,tag=!story-form_obsidian-ok,name="blue"] add story-form_obsidian
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-form_obsidian-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-form_obsidian

execute if entity @a[limit=1,team=blue,advancements={story/mine_diamond=true}] run advancement grant @a[team=blue] only minecraft:story/mine_diamond
execute if entity @a[limit=1,team=blue,advancements={story/mine_diamond=true}] run tag @e[type=armor_stand,tag=!story-mine_diamond-ok,name="blue"] add story-mine_diamond
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-mine_diamond-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-mine_diamond

execute if entity @a[limit=1,team=blue,advancements={story/enter_the_nether=true}] run advancement grant @a[team=blue] only minecraft:story/enter_the_nether
execute if entity @a[limit=1,team=blue,advancements={story/enter_the_nether=true}] run tag @e[type=armor_stand,tag=!story-enter_the_nether-ok,name="blue"] add story-enter_the_nether
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-enter_the_nether-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-enter_the_nether

execute if entity @a[limit=1,team=blue,advancements={story/shiny_gear=true}] run advancement grant @a[team=blue] only minecraft:story/shiny_gear
execute if entity @a[limit=1,team=blue,advancements={story/shiny_gear=true}] run tag @e[type=armor_stand,tag=!story-shiny_gear-ok,name="blue"] add story-shiny_gear
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-shiny_gear-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-shiny_gear

execute if entity @a[limit=1,team=blue,advancements={story/enchant_item=true}] run advancement grant @a[team=blue] only minecraft:story/enchant_item
execute if entity @a[limit=1,team=blue,advancements={story/enchant_item=true}] run tag @e[type=armor_stand,tag=!story-enchant_item-ok,name="blue"] add story-enchant_item
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-enchant_item-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-enchant_item

execute if entity @a[limit=1,team=blue,advancements={story/cure_zombie_villager=true}] run advancement grant @a[team=blue] only minecraft:story/cure_zombie_villager
execute if entity @a[limit=1,team=blue,advancements={story/cure_zombie_villager=true}] run tag @e[type=armor_stand,tag=!story-cure_zombie_villager-ok,name="blue"] add story-cure_zombie_villager
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-cure_zombie_villager-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-cure_zombie_villager

execute if entity @a[limit=1,team=blue,advancements={story/follow_ender_eye=true}] run advancement grant @a[team=blue] only minecraft:story/follow_ender_eye
execute if entity @a[limit=1,team=blue,advancements={story/follow_ender_eye=true}] run tag @e[type=armor_stand,tag=!story-follow_ender_eye-ok,name="blue"] add story-follow_ender_eye
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-follow_ender_eye-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-follow_ender_eye

execute if entity @a[limit=1,team=blue,advancements={story/enter_the_end=true}] run advancement grant @a[team=blue] only minecraft:story/enter_the_end
execute if entity @a[limit=1,team=blue,advancements={story/enter_the_end=true}] run tag @e[type=armor_stand,tag=!story-enter_the_end-ok,name="blue"] add story-enter_the_end
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="blue"] run tag @e[type=armor_stand,name="blue"] add story-enter_the_end-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="blue"] run tag @e[type=armor_stand,name="blue"] remove story-enter_the_end

execute if entity @a[limit=1,team=blue,advancements={nether/root=true}] run advancement grant @a[team=blue] only minecraft:nether/root
execute if entity @a[limit=1,team=blue,advancements={nether/root=true}] run tag @e[type=armor_stand,tag=!nether-root-ok,name="blue"] add nether-root
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-root

execute if entity @a[limit=1,team=blue,advancements={nether/return_to_sender=true}] run advancement grant @a[team=blue] only minecraft:nether/return_to_sender
execute if entity @a[limit=1,team=blue,advancements={nether/return_to_sender=true}] run tag @e[type=armor_stand,tag=!nether-return_to_sender-ok,name="blue"] add nether-return_to_sender
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-return_to_sender-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-return_to_sender

execute if entity @a[limit=1,team=blue,advancements={nether/find_bastion=true}] run advancement grant @a[team=blue] only minecraft:nether/find_bastion
execute if entity @a[limit=1,team=blue,advancements={nether/find_bastion=true}] run tag @e[type=armor_stand,tag=!nether-find_bastion-ok,name="blue"] add nether-find_bastion
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-find_bastion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-find_bastion

execute if entity @a[limit=1,team=blue,advancements={nether/obtain_ancient_debris=true}] run advancement grant @a[team=blue] only minecraft:nether/obtain_ancient_debris
execute if entity @a[limit=1,team=blue,advancements={nether/obtain_ancient_debris=true}] run tag @e[type=armor_stand,tag=!nether-obtain_ancient_debris-ok,name="blue"] add nether-obtain_ancient_debris
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-obtain_ancient_debris-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-obtain_ancient_debris

execute if entity @a[limit=1,team=blue,advancements={nether/fast_travel=true}] run advancement grant @a[team=blue] only minecraft:nether/fast_travel
execute if entity @a[limit=1,team=blue,advancements={nether/fast_travel=true}] run tag @e[type=armor_stand,tag=!nether-fast_travel-ok,name="blue"] add nether-fast_travel
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-fast_travel-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-fast_travel

execute if entity @a[limit=1,team=blue,advancements={nether/find_fortress=true}] run advancement grant @a[team=blue] only minecraft:nether/find_fortress
execute if entity @a[limit=1,team=blue,advancements={nether/find_fortress=true}] run tag @e[type=armor_stand,tag=!nether-find_fortress-ok,name="blue"] add nether-find_fortress
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-find_fortress-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-find_fortress

execute if entity @a[limit=1,team=blue,advancements={nether/obtain_crying_obsidian=true}] run advancement grant @a[team=blue] only minecraft:nether/obtain_crying_obsidian
execute if entity @a[limit=1,team=blue,advancements={nether/obtain_crying_obsidian=true}] run tag @e[type=armor_stand,tag=!nether-obtain_crying_obsidian-ok,name="blue"] add nether-obtain_crying_obsidian
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-obtain_crying_obsidian-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-obtain_crying_obsidian

execute if entity @a[limit=1,team=blue,advancements={nether/distract_piglin=true}] run advancement grant @a[team=blue] only minecraft:nether/distract_piglin
execute if entity @a[limit=1,team=blue,advancements={nether/distract_piglin=true}] run tag @e[type=armor_stand,tag=!nether-distract_piglin-ok,name="blue"] add nether-distract_piglin
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-distract_piglin-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-distract_piglin

execute if entity @a[limit=1,team=blue,advancements={nether/ride_strider=true}] run advancement grant @a[team=blue] only minecraft:nether/ride_strider
execute if entity @a[limit=1,team=blue,advancements={nether/ride_strider=true}] run tag @e[type=armor_stand,tag=!nether-ride_strider-ok,name="blue"] add nether-ride_strider
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-ride_strider-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-ride_strider

execute if entity @a[limit=1,team=blue,advancements={nether/uneasy_alliance=true}] run advancement grant @a[team=blue] only minecraft:nether/uneasy_alliance
execute if entity @a[limit=1,team=blue,advancements={nether/uneasy_alliance=true}] run tag @e[type=armor_stand,tag=!nether-uneasy_alliance-ok,name="blue"] add nether-uneasy_alliance
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-uneasy_alliance-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-uneasy_alliance

execute if entity @a[limit=1,team=blue,advancements={nether/loot_bastion=true}] run advancement grant @a[team=blue] only minecraft:nether/loot_bastion
execute if entity @a[limit=1,team=blue,advancements={nether/loot_bastion=true}] run tag @e[type=armor_stand,tag=!nether-loot_bastion-ok,name="blue"] add nether-loot_bastion
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-loot_bastion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-loot_bastion

execute if entity @a[limit=1,team=blue,advancements={nether/use_lodestone=true}] run advancement grant @a[team=blue] only minecraft:nether/use_lodestone
execute if entity @a[limit=1,team=blue,advancements={nether/use_lodestone=true}] run tag @e[type=armor_stand,tag=!nether-use_lodestone-ok,name="blue"] add nether-use_lodestone
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-use_lodestone-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-use_lodestone

execute if entity @a[limit=1,team=blue,advancements={nether/netherite_armor=true}] run advancement grant @a[team=blue] only minecraft:nether/netherite_armor
execute if entity @a[limit=1,team=blue,advancements={nether/netherite_armor=true}] run tag @e[type=armor_stand,tag=!nether-netherite_armor-ok,name="blue"] add nether-netherite_armor
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-netherite_armor-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-netherite_armor

execute if entity @a[limit=1,team=blue,advancements={nether/get_wither_skull=true}] run advancement grant @a[team=blue] only minecraft:nether/get_wither_skull
execute if entity @a[limit=1,team=blue,advancements={nether/get_wither_skull=true}] run tag @e[type=armor_stand,tag=!nether-get_wither_skull-ok,name="blue"] add nether-get_wither_skull
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-get_wither_skull-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-get_wither_skull

execute if entity @a[limit=1,team=blue,advancements={nether/obtain_blaze_rod=true}] run advancement grant @a[team=blue] only minecraft:nether/obtain_blaze_rod
execute if entity @a[limit=1,team=blue,advancements={nether/obtain_blaze_rod=true}] run tag @e[type=armor_stand,tag=!nether-obtain_blaze_rod-ok,name="blue"] add nether-obtain_blaze_rod
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-obtain_blaze_rod-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-obtain_blaze_rod

execute if entity @a[limit=1,team=blue,advancements={nether/charge_respawn_anchor=true}] run advancement grant @a[team=blue] only minecraft:nether/charge_respawn_anchor
execute if entity @a[limit=1,team=blue,advancements={nether/charge_respawn_anchor=true}] run tag @e[type=armor_stand,tag=!nether-charge_respawn_anchor-ok,name="blue"] add nether-charge_respawn_anchor
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-charge_respawn_anchor-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-charge_respawn_anchor

execute if entity @a[limit=1,team=blue,advancements={nether/ride_strider_in_overworld_lava=true}] run advancement grant @a[team=blue] only minecraft:nether/ride_strider_in_overworld_lava
execute if entity @a[limit=1,team=blue,advancements={nether/ride_strider_in_overworld_lava=true}] run tag @e[type=armor_stand,tag=!nether-ride_strider_in_overworld_lava-ok,name="blue"] add nether-ride_strider_in_overworld_lava
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-ride_strider_in_overworld_lava-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-ride_strider_in_overworld_lava

execute if entity @a[limit=1,team=blue,advancements={nether/explore_nether=true}] run advancement grant @a[team=blue] only minecraft:nether/explore_nether
execute if entity @a[limit=1,team=blue,advancements={nether/explore_nether=true}] run tag @e[type=armor_stand,tag=!nether-explore_nether-ok,name="blue"] add nether-explore_nether
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-explore_nether-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-explore_nether

execute if entity @a[limit=1,team=blue,advancements={nether/summon_wither=true}] run advancement grant @a[team=blue] only minecraft:nether/summon_wither
execute if entity @a[limit=1,team=blue,advancements={nether/summon_wither=true}] run tag @e[type=armor_stand,tag=!nether-summon_wither-ok,name="blue"] add nether-summon_wither
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-summon_wither-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-summon_wither

execute if entity @a[limit=1,team=blue,advancements={nether/brew_potion=true}] run advancement grant @a[team=blue] only minecraft:nether/brew_potion
execute if entity @a[limit=1,team=blue,advancements={nether/brew_potion=true}] run tag @e[type=armor_stand,tag=!nether-brew_potion-ok,name="blue"] add nether-brew_potion
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-brew_potion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-brew_potion

execute if entity @a[limit=1,team=blue,advancements={nether/create_beacon=true}] run advancement grant @a[team=blue] only minecraft:nether/create_beacon
execute if entity @a[limit=1,team=blue,advancements={nether/create_beacon=true}] run tag @e[type=armor_stand,tag=!nether-create_beacon-ok,name="blue"] add nether-create_beacon
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-create_beacon-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-create_beacon

execute if entity @a[limit=1,team=blue,advancements={nether/all_potions=true}] run advancement grant @a[team=blue] only minecraft:nether/all_potions
execute if entity @a[limit=1,team=blue,advancements={nether/all_potions=true}] run tag @e[type=armor_stand,tag=!nether-all_potions-ok,name="blue"] add nether-all_potions
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-all_potions-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-all_potions

execute if entity @a[limit=1,team=blue,advancements={nether/create_full_beacon=true}] run advancement grant @a[team=blue] only minecraft:nether/create_full_beacon
execute if entity @a[limit=1,team=blue,advancements={nether/create_full_beacon=true}] run tag @e[type=armor_stand,tag=!nether-create_full_beacon-ok,name="blue"] add nether-create_full_beacon
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-create_full_beacon-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-create_full_beacon

execute if entity @a[limit=1,team=blue,advancements={nether/all_effects=true}] run advancement grant @a[team=blue] only minecraft:nether/all_effects
execute if entity @a[limit=1,team=blue,advancements={nether/all_effects=true}] run tag @e[type=armor_stand,tag=!nether-all_effects-ok,name="blue"] add nether-all_effects
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="blue"] run tag @e[type=armor_stand,name="blue"] add nether-all_effects-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="blue"] run tag @e[type=armor_stand,name="blue"] remove nether-all_effects

execute if entity @a[limit=1,team=blue,advancements={end/root=true}] run advancement grant @a[team=blue] only minecraft:end/root
execute if entity @a[limit=1,team=blue,advancements={end/root=true}] run tag @e[type=armor_stand,tag=!end-root-ok,name="blue"] add end-root
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-root

execute if entity @a[limit=1,team=blue,advancements={end/kill_dragon=true}] run advancement grant @a[team=blue] only minecraft:end/kill_dragon
execute if entity @a[limit=1,team=blue,advancements={end/kill_dragon=true}] run tag @e[type=armor_stand,tag=!end-kill_dragon-ok,name="blue"] add end-kill_dragon
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-kill_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-kill_dragon

execute if entity @a[limit=1,team=blue,advancements={end/dragon_egg=true}] run advancement grant @a[team=blue] only minecraft:end/dragon_egg
execute if entity @a[limit=1,team=blue,advancements={end/dragon_egg=true}] run tag @e[type=armor_stand,tag=!end-dragon_egg-ok,name="blue"] add end-dragon_egg
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-dragon_egg-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-dragon_egg

execute if entity @a[limit=1,team=blue,advancements={end/enter_end_gateway=true}] run advancement grant @a[team=blue] only minecraft:end/enter_end_gateway
execute if entity @a[limit=1,team=blue,advancements={end/enter_end_gateway=true}] run tag @e[type=armor_stand,tag=!end-enter_end_gateway-ok,name="blue"] add end-enter_end_gateway
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-enter_end_gateway-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-enter_end_gateway

execute if entity @a[limit=1,team=blue,advancements={end/respawn_dragon=true}] run advancement grant @a[team=blue] only minecraft:end/respawn_dragon
execute if entity @a[limit=1,team=blue,advancements={end/respawn_dragon=true}] run tag @e[type=armor_stand,tag=!end-respawn_dragon-ok,name="blue"] add end-respawn_dragon
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-respawn_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-respawn_dragon

execute if entity @a[limit=1,team=blue,advancements={end/dragon_breath=true}] run advancement grant @a[team=blue] only minecraft:end/dragon_breath
execute if entity @a[limit=1,team=blue,advancements={end/dragon_breath=true}] run tag @e[type=armor_stand,tag=!end-dragon_breath-ok,name="blue"] add end-dragon_breath
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-dragon_breath-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-dragon_breath

execute if entity @a[limit=1,team=blue,advancements={end/find_end_city=true}] run advancement grant @a[team=blue] only minecraft:end/find_end_city
execute if entity @a[limit=1,team=blue,advancements={end/find_end_city=true}] run tag @e[type=armor_stand,tag=!end-find_end_city-ok,name="blue"] add end-find_end_city
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-find_end_city-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-find_end_city

execute if entity @a[limit=1,team=blue,advancements={end/elytra=true}] run advancement grant @a[team=blue] only minecraft:end/elytra
execute if entity @a[limit=1,team=blue,advancements={end/elytra=true}] run tag @e[type=armor_stand,tag=!end-elytra-ok,name="blue"] add end-elytra
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-elytra-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-elytra

execute if entity @a[limit=1,team=blue,advancements={end/levitate=true}] run advancement grant @a[team=blue] only minecraft:end/levitate
execute if entity @a[limit=1,team=blue,advancements={end/levitate=true}] run tag @e[type=armor_stand,tag=!end-levitate-ok,name="blue"] add end-levitate
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="blue"] run tag @e[type=armor_stand,name="blue"] add end-levitate-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="blue"] run tag @e[type=armor_stand,name="blue"] remove end-levitate

execute if entity @a[limit=1,team=blue,advancements={adventure/root=true}] run advancement grant @a[team=blue] only minecraft:adventure/root
execute if entity @a[limit=1,team=blue,advancements={adventure/root=true}] run tag @e[type=armor_stand,tag=!adventure-root-ok,name="blue"] add adventure-root
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-root

execute if entity @a[limit=1,team=blue,advancements={adventure/voluntary_exile=true}] run advancement grant @a[team=blue] only minecraft:adventure/voluntary_exile
execute if entity @a[limit=1,team=blue,advancements={adventure/voluntary_exile=true}] run tag @e[type=armor_stand,tag=!adventure-voluntary_exile-ok,name="blue"] add adventure-voluntary_exile
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-voluntary_exile-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-voluntary_exile

execute if entity @a[limit=1,team=blue,advancements={adventure/spyglass_at_parrot=true}] run advancement grant @a[team=blue] only minecraft:adventure/spyglass_at_parrot
execute if entity @a[limit=1,team=blue,advancements={adventure/spyglass_at_parrot=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_parrot-ok,name="blue"] add adventure-spyglass_at_parrot
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-spyglass_at_parrot-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-spyglass_at_parrot

execute if entity @a[limit=1,team=blue,advancements={adventure/kill_a_mob=true}] run advancement grant @a[team=blue] only minecraft:adventure/kill_a_mob
execute if entity @a[limit=1,team=blue,advancements={adventure/kill_a_mob=true}] run tag @e[type=armor_stand,tag=!adventure-kill_a_mob-ok,name="blue"] add adventure-kill_a_mob
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-kill_a_mob-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-kill_a_mob

execute if entity @a[limit=1,team=blue,advancements={adventure/trade=true}] run advancement grant @a[team=blue] only minecraft:adventure/trade
execute if entity @a[limit=1,team=blue,advancements={adventure/trade=true}] run tag @e[type=armor_stand,tag=!adventure-trade-ok,name="blue"] add adventure-trade
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-trade-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-trade

execute if entity @a[limit=1,team=blue,advancements={adventure/honey_block_slide=true}] run advancement grant @a[team=blue] only minecraft:adventure/honey_block_slide
execute if entity @a[limit=1,team=blue,advancements={adventure/honey_block_slide=true}] run tag @e[type=armor_stand,tag=!adventure-honey_block_slide-ok,name="blue"] add adventure-honey_block_slide
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-honey_block_slide-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-honey_block_slide

execute if entity @a[limit=1,team=blue,advancements={adventure/ol_betsy=true}] run advancement grant @a[team=blue] only minecraft:adventure/ol_betsy
execute if entity @a[limit=1,team=blue,advancements={adventure/ol_betsy=true}] run tag @e[type=armor_stand,tag=!adventure-ol_betsy-ok,name="blue"] add adventure-ol_betsy
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-ol_betsy-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-ol_betsy

execute if entity @a[limit=1,team=blue,advancements={adventure/lightning_rod_with_villager_no_fire=true}] run advancement grant @a[team=blue] only minecraft:adventure/lightning_rod_with_villager_no_fire
execute if entity @a[limit=1,team=blue,advancements={adventure/lightning_rod_with_villager_no_fire=true}] run tag @e[type=armor_stand,tag=!adventure-lightning_rod_with_villager_no_fire-ok,name="blue"] add adventure-lightning_rod_with_villager_no_fire
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-lightning_rod_with_villager_no_fire-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-lightning_rod_with_villager_no_fire

execute if entity @a[limit=1,team=blue,advancements={adventure/fall_from_world_height=true}] run advancement grant @a[team=blue] only minecraft:adventure/fall_from_world_height
execute if entity @a[limit=1,team=blue,advancements={adventure/fall_from_world_height=true}] run tag @e[type=armor_stand,tag=!adventure-fall_from_world_height-ok,name="blue"] add adventure-fall_from_world_height
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-fall_from_world_height-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-fall_from_world_height

execute if entity @a[limit=1,team=blue,advancements={adventure/avoid_vibration=true}] run advancement grant @a[team=blue] only minecraft:adventure/avoid_vibration
execute if entity @a[limit=1,team=blue,advancements={adventure/avoid_vibration=true}] run tag @e[type=armor_stand,tag=!adventure-avoid_vibration-ok,name="blue"] add adventure-avoid_vibration
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-avoid_vibration-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-avoid_vibration

execute if entity @a[limit=1,team=blue,advancements={adventure/sleep_in_bed=true}] run advancement grant @a[team=blue] only minecraft:adventure/sleep_in_bed
execute if entity @a[limit=1,team=blue,advancements={adventure/sleep_in_bed=true}] run tag @e[type=armor_stand,tag=!adventure-sleep_in_bed-ok,name="blue"] add adventure-sleep_in_bed
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-sleep_in_bed-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-sleep_in_bed

execute if entity @a[limit=1,team=blue,advancements={adventure/hero_of_the_village=true}] run advancement grant @a[team=blue] only minecraft:adventure/hero_of_the_village
execute if entity @a[limit=1,team=blue,advancements={adventure/hero_of_the_village=true}] run tag @e[type=armor_stand,tag=!adventure-hero_of_the_village-ok,name="blue"] add adventure-hero_of_the_village
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-hero_of_the_village-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-hero_of_the_village

execute if entity @a[limit=1,team=blue,advancements={adventure/spyglass_at_ghast=true}] run advancement grant @a[team=blue] only minecraft:adventure/spyglass_at_ghast
execute if entity @a[limit=1,team=blue,advancements={adventure/spyglass_at_ghast=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_ghast-ok,name="blue"] add adventure-spyglass_at_ghast
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-spyglass_at_ghast-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-spyglass_at_ghast

execute if entity @a[limit=1,team=blue,advancements={adventure/throw_trident=true}] run advancement grant @a[team=blue] only minecraft:adventure/throw_trident
execute if entity @a[limit=1,team=blue,advancements={adventure/throw_trident=true}] run tag @e[type=armor_stand,tag=!adventure-throw_trident-ok,name="blue"] add adventure-throw_trident
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-throw_trident-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-throw_trident

execute if entity @a[limit=1,team=blue,advancements={adventure/kill_mob_near_sculk_catalyst=true}] run advancement grant @a[team=blue] only minecraft:adventure/kill_mob_near_sculk_catalyst
execute if entity @a[limit=1,team=blue,advancements={adventure/kill_mob_near_sculk_catalyst=true}] run tag @e[type=armor_stand,tag=!adventure-kill_mob_near_sculk_catalyst-ok,name="blue"] add adventure-kill_mob_near_sculk_catalyst
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-kill_mob_near_sculk_catalyst-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-kill_mob_near_sculk_catalyst

execute if entity @a[limit=1,team=blue,advancements={adventure/shoot_arrow=true}] run advancement grant @a[team=blue] only minecraft:adventure/shoot_arrow
execute if entity @a[limit=1,team=blue,advancements={adventure/shoot_arrow=true}] run tag @e[type=armor_stand,tag=!adventure-shoot_arrow-ok,name="blue"] add adventure-shoot_arrow
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-shoot_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-shoot_arrow

execute if entity @a[limit=1,team=blue,advancements={adventure/kill_all_mobs=true}] run advancement grant @a[team=blue] only minecraft:adventure/kill_all_mobs
execute if entity @a[limit=1,team=blue,advancements={adventure/kill_all_mobs=true}] run tag @e[type=armor_stand,tag=!adventure-kill_all_mobs-ok,name="blue"] add adventure-kill_all_mobs
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-kill_all_mobs-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-kill_all_mobs

execute if entity @a[limit=1,team=blue,advancements={adventure/totem_of_undying=true}] run advancement grant @a[team=blue] only minecraft:adventure/totem_of_undying
execute if entity @a[limit=1,team=blue,advancements={adventure/totem_of_undying=true}] run tag @e[type=armor_stand,tag=!adventure-totem_of_undying-ok,name="blue"] add adventure-totem_of_undying
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-totem_of_undying-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-totem_of_undying

execute if entity @a[limit=1,team=blue,advancements={adventure/summon_iron_golem=true}] run advancement grant @a[team=blue] only minecraft:adventure/summon_iron_golem
execute if entity @a[limit=1,team=blue,advancements={adventure/summon_iron_golem=true}] run tag @e[type=armor_stand,tag=!adventure-summon_iron_golem-ok,name="blue"] add adventure-summon_iron_golem
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-summon_iron_golem-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-summon_iron_golem

execute if entity @a[limit=1,team=blue,advancements={adventure/trade_at_world_height=true}] run advancement grant @a[team=blue] only minecraft:adventure/trade_at_world_height
execute if entity @a[limit=1,team=blue,advancements={adventure/trade_at_world_height=true}] run tag @e[type=armor_stand,tag=!adventure-trade_at_world_height-ok,name="blue"] add adventure-trade_at_world_height
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-trade_at_world_height-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-trade_at_world_height

execute if entity @a[limit=1,team=blue,advancements={adventure/two_birds_one_arrow=true}] run advancement grant @a[team=blue] only minecraft:adventure/two_birds_one_arrow
execute if entity @a[limit=1,team=blue,advancements={adventure/two_birds_one_arrow=true}] run tag @e[type=armor_stand,tag=!adventure-two_birds_one_arrow-ok,name="blue"] add adventure-two_birds_one_arrow
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-two_birds_one_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-two_birds_one_arrow

execute if entity @a[limit=1,team=blue,advancements={adventure/whos_the_pillager_now=true}] run advancement grant @a[team=blue] only minecraft:adventure/whos_the_pillager_now
execute if entity @a[limit=1,team=blue,advancements={adventure/whos_the_pillager_now=true}] run tag @e[type=armor_stand,tag=!adventure-whos_the_pillager_now-ok,name="blue"] add adventure-whos_the_pillager_now
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-whos_the_pillager_now-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-whos_the_pillager_now

execute if entity @a[limit=1,team=blue,advancements={adventure/arbalistic=true}] run advancement grant @a[team=blue] only minecraft:adventure/arbalistic
execute if entity @a[limit=1,team=blue,advancements={adventure/arbalistic=true}] run tag @e[type=armor_stand,tag=!adventure-arbalistic-ok,name="blue"] add adventure-arbalistic
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-arbalistic-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-arbalistic

execute if entity @a[limit=1,team=blue,advancements={adventure/adventuring_time=true}] run advancement grant @a[team=blue] only minecraft:adventure/adventuring_time
execute if entity @a[limit=1,team=blue,advancements={adventure/adventuring_time=true}] run tag @e[type=armor_stand,tag=!adventure-adventuring_time-ok,name="blue"] add adventure-adventuring_time
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-adventuring_time-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-adventuring_time

execute if entity @a[limit=1,team=blue,advancements={adventure/play_jukebox_in_meadows=true}] run advancement grant @a[team=blue] only minecraft:adventure/play_jukebox_in_meadows
execute if entity @a[limit=1,team=blue,advancements={adventure/play_jukebox_in_meadows=true}] run tag @e[type=armor_stand,tag=!adventure-play_jukebox_in_meadows-ok,name="blue"] add adventure-play_jukebox_in_meadows
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-play_jukebox_in_meadows-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-play_jukebox_in_meadows

execute if entity @a[limit=1,team=blue,advancements={adventure/walk_on_powder_snow_with_leather_boots=true}] run advancement grant @a[team=blue] only minecraft:adventure/walk_on_powder_snow_with_leather_boots
execute if entity @a[limit=1,team=blue,advancements={adventure/walk_on_powder_snow_with_leather_boots=true}] run tag @e[type=armor_stand,tag=!adventure-walk_on_powder_snow_with_leather_boots-ok,name="blue"] add adventure-walk_on_powder_snow_with_leather_boots
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-walk_on_powder_snow_with_leather_boots-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-walk_on_powder_snow_with_leather_boots

execute if entity @a[limit=1,team=blue,advancements={adventure/spyglass_at_dragon=true}] run advancement grant @a[team=blue] only minecraft:adventure/spyglass_at_dragon
execute if entity @a[limit=1,team=blue,advancements={adventure/spyglass_at_dragon=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_dragon-ok,name="blue"] add adventure-spyglass_at_dragon
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-spyglass_at_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-spyglass_at_dragon

execute if entity @a[limit=1,team=blue,advancements={adventure/very_very_frightening=true}] run advancement grant @a[team=blue] only minecraft:adventure/very_very_frightening
execute if entity @a[limit=1,team=blue,advancements={adventure/very_very_frightening=true}] run tag @e[type=armor_stand,tag=!adventure-very_very_frightening-ok,name="blue"] add adventure-very_very_frightening
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-very_very_frightening-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-very_very_frightening

execute if entity @a[limit=1,team=blue,advancements={adventure/sniper_duel=true}] run advancement grant @a[team=blue] only minecraft:adventure/sniper_duel
execute if entity @a[limit=1,team=blue,advancements={adventure/sniper_duel=true}] run tag @e[type=armor_stand,tag=!adventure-sniper_duel-ok,name="blue"] add adventure-sniper_duel
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-sniper_duel-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-sniper_duel

execute if entity @a[limit=1,team=blue,advancements={adventure/bullseye=true}] run advancement grant @a[team=blue] only minecraft:adventure/bullseye
execute if entity @a[limit=1,team=blue,advancements={adventure/bullseye=true}] run tag @e[type=armor_stand,tag=!adventure-bullseye-ok,name="blue"] add adventure-bullseye
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="blue"] run tag @e[type=armor_stand,name="blue"] add adventure-bullseye-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="blue"] run tag @e[type=armor_stand,name="blue"] remove adventure-bullseye

execute if entity @a[limit=1,team=blue,advancements={husbandry/root=true}] run advancement grant @a[team=blue] only minecraft:husbandry/root
execute if entity @a[limit=1,team=blue,advancements={husbandry/root=true}] run tag @e[type=armor_stand,tag=!husbandry-root-ok,name="blue"] add husbandry-root
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-root

execute if entity @a[limit=1,team=blue,advancements={husbandry/safely_harvest_honey=true}] run advancement grant @a[team=blue] only minecraft:husbandry/safely_harvest_honey
execute if entity @a[limit=1,team=blue,advancements={husbandry/safely_harvest_honey=true}] run tag @e[type=armor_stand,tag=!husbandry-safely_harvest_honey-ok,name="blue"] add husbandry-safely_harvest_honey
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-safely_harvest_honey-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-safely_harvest_honey

execute if entity @a[limit=1,team=blue,advancements={husbandry/breed_an_animal=true}] run advancement grant @a[team=blue] only minecraft:husbandry/breed_an_animal
execute if entity @a[limit=1,team=blue,advancements={husbandry/breed_an_animal=true}] run tag @e[type=armor_stand,tag=!husbandry-breed_an_animal-ok,name="blue"] add husbandry-breed_an_animal
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-breed_an_animal-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-breed_an_animal

execute if entity @a[limit=1,team=blue,advancements={husbandry/allay_deliver_item_to_player=true}] run advancement grant @a[team=blue] only minecraft:husbandry/allay_deliver_item_to_player
execute if entity @a[limit=1,team=blue,advancements={husbandry/allay_deliver_item_to_player=true}] run tag @e[type=armor_stand,tag=!husbandry-allay_deliver_item_to_player-ok,name="blue"] add husbandry-allay_deliver_item_to_player
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-allay_deliver_item_to_player-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-allay_deliver_item_to_player

execute if entity @a[limit=1,team=blue,advancements={husbandry/ride_a_boat_with_a_goat=true}] run advancement grant @a[team=blue] only minecraft:husbandry/ride_a_boat_with_a_goat
execute if entity @a[limit=1,team=blue,advancements={husbandry/ride_a_boat_with_a_goat=true}] run tag @e[type=armor_stand,tag=!husbandry-ride_a_boat_with_a_goat-ok,name="blue"] add husbandry-ride_a_boat_with_a_goat
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-ride_a_boat_with_a_goat-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-ride_a_boat_with_a_goat

execute if entity @a[limit=1,team=blue,advancements={husbandry/tame_an_animal=true}] run advancement grant @a[team=blue] only minecraft:husbandry/tame_an_animal
execute if entity @a[limit=1,team=blue,advancements={husbandry/tame_an_animal=true}] run tag @e[type=armor_stand,tag=!husbandry-tame_an_animal-ok,name="blue"] add husbandry-tame_an_animal
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-tame_an_animal-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-tame_an_animal

execute if entity @a[limit=1,team=blue,advancements={husbandry/make_a_sign_glow=true}] run advancement grant @a[team=blue] only minecraft:husbandry/make_a_sign_glow
execute if entity @a[limit=1,team=blue,advancements={husbandry/make_a_sign_glow=true}] run tag @e[type=armor_stand,tag=!husbandry-make_a_sign_glow-ok,name="blue"] add husbandry-make_a_sign_glow
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-make_a_sign_glow-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-make_a_sign_glow

execute if entity @a[limit=1,team=blue,advancements={husbandry/fishy_business=true}] run advancement grant @a[team=blue] only minecraft:husbandry/fishy_business
execute if entity @a[limit=1,team=blue,advancements={husbandry/fishy_business=true}] run tag @e[type=armor_stand,tag=!husbandry-fishy_business-ok,name="blue"] add husbandry-fishy_business
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-fishy_business-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-fishy_business

execute if entity @a[limit=1,team=blue,advancements={husbandry/silk_touch_nest=true}] run advancement grant @a[team=blue] only minecraft:husbandry/silk_touch_nest
execute if entity @a[limit=1,team=blue,advancements={husbandry/silk_touch_nest=true}] run tag @e[type=armor_stand,tag=!husbandry-silk_touch_nest-ok,name="blue"] add husbandry-silk_touch_nest
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-silk_touch_nest-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-silk_touch_nest

execute if entity @a[limit=1,team=blue,advancements={husbandry/tadpole_in_a_bucket=true}] run advancement grant @a[team=blue] only minecraft:husbandry/tadpole_in_a_bucket
execute if entity @a[limit=1,team=blue,advancements={husbandry/tadpole_in_a_bucket=true}] run tag @e[type=armor_stand,tag=!husbandry-tadpole_in_a_bucket-ok,name="blue"] add husbandry-tadpole_in_a_bucket
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-tadpole_in_a_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-tadpole_in_a_bucket

execute if entity @a[limit=1,team=blue,advancements={husbandry/plant_seed=true}] run advancement grant @a[team=blue] only minecraft:husbandry/plant_seed
execute if entity @a[limit=1,team=blue,advancements={husbandry/plant_seed=true}] run tag @e[type=armor_stand,tag=!husbandry-plant_seed-ok,name="blue"] add husbandry-plant_seed
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-plant_seed-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-plant_seed

execute if entity @a[limit=1,team=blue,advancements={husbandry/wax_on=true}] run advancement grant @a[team=blue] only minecraft:husbandry/wax_on
execute if entity @a[limit=1,team=blue,advancements={husbandry/wax_on=true}] run tag @e[type=armor_stand,tag=!husbandry-wax_on-ok,name="blue"] add husbandry-wax_on
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-wax_on-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-wax_on

execute if entity @a[limit=1,team=blue,advancements={husbandry/bred_all_animals=true}] run advancement grant @a[team=blue] only minecraft:husbandry/bred_all_animals
execute if entity @a[limit=1,team=blue,advancements={husbandry/bred_all_animals=true}] run tag @e[type=armor_stand,tag=!husbandry-bred_all_animals-ok,name="blue"] add husbandry-bred_all_animals
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-bred_all_animals-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-bred_all_animals

execute if entity @a[limit=1,team=blue,advancements={husbandry/allay_deliver_cake_to_note_block=true}] run advancement grant @a[team=blue] only minecraft:husbandry/allay_deliver_cake_to_note_block
execute if entity @a[limit=1,team=blue,advancements={husbandry/allay_deliver_cake_to_note_block=true}] run tag @e[type=armor_stand,tag=!husbandry-allay_deliver_cake_to_note_block-ok,name="blue"] add husbandry-allay_deliver_cake_to_note_block
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-allay_deliver_cake_to_note_block-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-allay_deliver_cake_to_note_block

execute if entity @a[limit=1,team=blue,advancements={husbandry/complete_catalogue=true}] run advancement grant @a[team=blue] only minecraft:husbandry/complete_catalogue
execute if entity @a[limit=1,team=blue,advancements={husbandry/complete_catalogue=true}] run tag @e[type=armor_stand,tag=!husbandry-complete_catalogue-ok,name="blue"] add husbandry-complete_catalogue
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-complete_catalogue-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-complete_catalogue

execute if entity @a[limit=1,team=blue,advancements={husbandry/tactical_fishing=true}] run advancement grant @a[team=blue] only minecraft:husbandry/tactical_fishing
execute if entity @a[limit=1,team=blue,advancements={husbandry/tactical_fishing=true}] run tag @e[type=armor_stand,tag=!husbandry-tactical_fishing-ok,name="blue"] add husbandry-tactical_fishing
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-tactical_fishing-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-tactical_fishing

execute if entity @a[limit=1,team=blue,advancements={husbandry/leash_all_frog_variants=true}] run advancement grant @a[team=blue] only minecraft:husbandry/leash_all_frog_variants
execute if entity @a[limit=1,team=blue,advancements={husbandry/leash_all_frog_variants=true}] run tag @e[type=armor_stand,tag=!husbandry-leash_all_frog_variants-ok,name="blue"] add husbandry-leash_all_frog_variants
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-leash_all_frog_variants-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-leash_all_frog_variants

execute if entity @a[limit=1,team=blue,advancements={husbandry/balanced_diet=true}] run advancement grant @a[team=blue] only minecraft:husbandry/balanced_diet
execute if entity @a[limit=1,team=blue,advancements={husbandry/balanced_diet=true}] run tag @e[type=armor_stand,tag=!husbandry-balanced_diet-ok,name="blue"] add husbandry-balanced_diet
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-balanced_diet-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-balanced_diet

execute if entity @a[limit=1,team=blue,advancements={husbandry/obtain_netherite_hoe=true}] run advancement grant @a[team=blue] only minecraft:husbandry/obtain_netherite_hoe
execute if entity @a[limit=1,team=blue,advancements={husbandry/obtain_netherite_hoe=true}] run tag @e[type=armor_stand,tag=!husbandry-obtain_netherite_hoe-ok,name="blue"] add husbandry-obtain_netherite_hoe
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-obtain_netherite_hoe-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-obtain_netherite_hoe

execute if entity @a[limit=1,team=blue,advancements={husbandry/wax_off=true}] run advancement grant @a[team=blue] only minecraft:husbandry/wax_off
execute if entity @a[limit=1,team=blue,advancements={husbandry/wax_off=true}] run tag @e[type=armor_stand,tag=!husbandry-wax_off-ok,name="blue"] add husbandry-wax_off
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-wax_off-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-wax_off

execute if entity @a[limit=1,team=blue,advancements={husbandry/axolotl_in_a_bucket=true}] run advancement grant @a[team=blue] only minecraft:husbandry/axolotl_in_a_bucket
execute if entity @a[limit=1,team=blue,advancements={husbandry/axolotl_in_a_bucket=true}] run tag @e[type=armor_stand,tag=!husbandry-axolotl_in_a_bucket-ok,name="blue"] add husbandry-axolotl_in_a_bucket
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-axolotl_in_a_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-axolotl_in_a_bucket

execute if entity @a[limit=1,team=blue,advancements={husbandry/froglights=true}] run advancement grant @a[team=blue] only minecraft:husbandry/froglights
execute if entity @a[limit=1,team=blue,advancements={husbandry/froglights=true}] run tag @e[type=armor_stand,tag=!husbandry-froglights-ok,name="blue"] add husbandry-froglights
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-froglights-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-froglights

execute if entity @a[limit=1,team=blue,advancements={husbandry/kill_axolotl_target=true}] run advancement grant @a[team=blue] only minecraft:husbandry/kill_axolotl_target
execute if entity @a[limit=1,team=blue,advancements={husbandry/kill_axolotl_target=true}] run tag @e[type=armor_stand,tag=!husbandry-kill_axolotl_target-ok,name="blue"] add husbandry-kill_axolotl_target
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="blue"] run scoreboard players add blue Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="blue"] run tag @e[type=armor_stand,name="blue"] add husbandry-kill_axolotl_target-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="blue"] run tag @e[type=armor_stand,name="blue"] remove husbandry-kill_axolotl_target

schedule function bingo:blue 1s