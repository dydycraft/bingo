execute if entity @a[limit=1,team=dark_aqua,advancements={story/root=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/root
execute if entity @a[limit=1,team=dark_aqua,advancements={story/root=true}] run tag @e[type=armor_stand,tag=!story-root-ok,name="dark_aqua"] add story-root
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-root

execute if entity @a[limit=1,team=dark_aqua,advancements={story/mine_stone=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/mine_stone
execute if entity @a[limit=1,team=dark_aqua,advancements={story/mine_stone=true}] run tag @e[type=armor_stand,tag=!story-mine_stone-ok,name="dark_aqua"] add story-mine_stone
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-mine_stone-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-mine_stone

execute if entity @a[limit=1,team=dark_aqua,advancements={story/upgrade_tools=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/upgrade_tools
execute if entity @a[limit=1,team=dark_aqua,advancements={story/upgrade_tools=true}] run tag @e[type=armor_stand,tag=!story-upgrade_tools-ok,name="dark_aqua"] add story-upgrade_tools
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-upgrade_tools-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-upgrade_tools

execute if entity @a[limit=1,team=dark_aqua,advancements={story/smelt_iron=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/smelt_iron
execute if entity @a[limit=1,team=dark_aqua,advancements={story/smelt_iron=true}] run tag @e[type=armor_stand,tag=!story-smelt_iron-ok,name="dark_aqua"] add story-smelt_iron
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-smelt_iron-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-smelt_iron

execute if entity @a[limit=1,team=dark_aqua,advancements={story/obtain_armor=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/obtain_armor
execute if entity @a[limit=1,team=dark_aqua,advancements={story/obtain_armor=true}] run tag @e[type=armor_stand,tag=!story-obtain_armor-ok,name="dark_aqua"] add story-obtain_armor
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-obtain_armor-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-obtain_armor

execute if entity @a[limit=1,team=dark_aqua,advancements={story/lava_bucket=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/lava_bucket
execute if entity @a[limit=1,team=dark_aqua,advancements={story/lava_bucket=true}] run tag @e[type=armor_stand,tag=!story-lava_bucket-ok,name="dark_aqua"] add story-lava_bucket
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-lava_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-lava_bucket

execute if entity @a[limit=1,team=dark_aqua,advancements={story/iron_tools=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/iron_tools
execute if entity @a[limit=1,team=dark_aqua,advancements={story/iron_tools=true}] run tag @e[type=armor_stand,tag=!story-iron_tools-ok,name="dark_aqua"] add story-iron_tools
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-iron_tools-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-iron_tools

execute if entity @a[limit=1,team=dark_aqua,advancements={story/deflect_arrow=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/deflect_arrow
execute if entity @a[limit=1,team=dark_aqua,advancements={story/deflect_arrow=true}] run tag @e[type=armor_stand,tag=!story-deflect_arrow-ok,name="dark_aqua"] add story-deflect_arrow
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-deflect_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-deflect_arrow

execute if entity @a[limit=1,team=dark_aqua,advancements={story/form_obsidian=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/form_obsidian
execute if entity @a[limit=1,team=dark_aqua,advancements={story/form_obsidian=true}] run tag @e[type=armor_stand,tag=!story-form_obsidian-ok,name="dark_aqua"] add story-form_obsidian
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-form_obsidian-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-form_obsidian

execute if entity @a[limit=1,team=dark_aqua,advancements={story/mine_diamond=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/mine_diamond
execute if entity @a[limit=1,team=dark_aqua,advancements={story/mine_diamond=true}] run tag @e[type=armor_stand,tag=!story-mine_diamond-ok,name="dark_aqua"] add story-mine_diamond
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-mine_diamond-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-mine_diamond

execute if entity @a[limit=1,team=dark_aqua,advancements={story/enter_the_nether=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/enter_the_nether
execute if entity @a[limit=1,team=dark_aqua,advancements={story/enter_the_nether=true}] run tag @e[type=armor_stand,tag=!story-enter_the_nether-ok,name="dark_aqua"] add story-enter_the_nether
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-enter_the_nether-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-enter_the_nether

execute if entity @a[limit=1,team=dark_aqua,advancements={story/shiny_gear=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/shiny_gear
execute if entity @a[limit=1,team=dark_aqua,advancements={story/shiny_gear=true}] run tag @e[type=armor_stand,tag=!story-shiny_gear-ok,name="dark_aqua"] add story-shiny_gear
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-shiny_gear-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-shiny_gear

execute if entity @a[limit=1,team=dark_aqua,advancements={story/enchant_item=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/enchant_item
execute if entity @a[limit=1,team=dark_aqua,advancements={story/enchant_item=true}] run tag @e[type=armor_stand,tag=!story-enchant_item-ok,name="dark_aqua"] add story-enchant_item
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-enchant_item-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-enchant_item

execute if entity @a[limit=1,team=dark_aqua,advancements={story/cure_zombie_villager=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/cure_zombie_villager
execute if entity @a[limit=1,team=dark_aqua,advancements={story/cure_zombie_villager=true}] run tag @e[type=armor_stand,tag=!story-cure_zombie_villager-ok,name="dark_aqua"] add story-cure_zombie_villager
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-cure_zombie_villager-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-cure_zombie_villager

execute if entity @a[limit=1,team=dark_aqua,advancements={story/follow_ender_eye=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/follow_ender_eye
execute if entity @a[limit=1,team=dark_aqua,advancements={story/follow_ender_eye=true}] run tag @e[type=armor_stand,tag=!story-follow_ender_eye-ok,name="dark_aqua"] add story-follow_ender_eye
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-follow_ender_eye-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-follow_ender_eye

execute if entity @a[limit=1,team=dark_aqua,advancements={story/enter_the_end=true}] run advancement grant @a[team=dark_aqua] only minecraft:story/enter_the_end
execute if entity @a[limit=1,team=dark_aqua,advancements={story/enter_the_end=true}] run tag @e[type=armor_stand,tag=!story-enter_the_end-ok,name="dark_aqua"] add story-enter_the_end
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add story-enter_the_end-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove story-enter_the_end

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/root=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/root
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/root=true}] run tag @e[type=armor_stand,tag=!nether-root-ok,name="dark_aqua"] add nether-root
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-root

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/return_to_sender=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/return_to_sender
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/return_to_sender=true}] run tag @e[type=armor_stand,tag=!nether-return_to_sender-ok,name="dark_aqua"] add nether-return_to_sender
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-return_to_sender-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-return_to_sender

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/find_bastion=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/find_bastion
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/find_bastion=true}] run tag @e[type=armor_stand,tag=!nether-find_bastion-ok,name="dark_aqua"] add nether-find_bastion
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-find_bastion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-find_bastion

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/obtain_ancient_debris=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/obtain_ancient_debris
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/obtain_ancient_debris=true}] run tag @e[type=armor_stand,tag=!nether-obtain_ancient_debris-ok,name="dark_aqua"] add nether-obtain_ancient_debris
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-obtain_ancient_debris-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-obtain_ancient_debris

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/fast_travel=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/fast_travel
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/fast_travel=true}] run tag @e[type=armor_stand,tag=!nether-fast_travel-ok,name="dark_aqua"] add nether-fast_travel
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-fast_travel-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-fast_travel

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/find_fortress=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/find_fortress
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/find_fortress=true}] run tag @e[type=armor_stand,tag=!nether-find_fortress-ok,name="dark_aqua"] add nether-find_fortress
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-find_fortress-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-find_fortress

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/obtain_crying_obsidian=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/obtain_crying_obsidian
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/obtain_crying_obsidian=true}] run tag @e[type=armor_stand,tag=!nether-obtain_crying_obsidian-ok,name="dark_aqua"] add nether-obtain_crying_obsidian
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-obtain_crying_obsidian-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-obtain_crying_obsidian

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/distract_piglin=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/distract_piglin
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/distract_piglin=true}] run tag @e[type=armor_stand,tag=!nether-distract_piglin-ok,name="dark_aqua"] add nether-distract_piglin
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-distract_piglin-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-distract_piglin

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/ride_strider=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/ride_strider
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/ride_strider=true}] run tag @e[type=armor_stand,tag=!nether-ride_strider-ok,name="dark_aqua"] add nether-ride_strider
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-ride_strider-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-ride_strider

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/uneasy_alliance=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/uneasy_alliance
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/uneasy_alliance=true}] run tag @e[type=armor_stand,tag=!nether-uneasy_alliance-ok,name="dark_aqua"] add nether-uneasy_alliance
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-uneasy_alliance-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-uneasy_alliance

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/loot_bastion=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/loot_bastion
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/loot_bastion=true}] run tag @e[type=armor_stand,tag=!nether-loot_bastion-ok,name="dark_aqua"] add nether-loot_bastion
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-loot_bastion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-loot_bastion

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/use_lodestone=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/use_lodestone
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/use_lodestone=true}] run tag @e[type=armor_stand,tag=!nether-use_lodestone-ok,name="dark_aqua"] add nether-use_lodestone
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-use_lodestone-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-use_lodestone

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/netherite_armor=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/netherite_armor
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/netherite_armor=true}] run tag @e[type=armor_stand,tag=!nether-netherite_armor-ok,name="dark_aqua"] add nether-netherite_armor
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-netherite_armor-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-netherite_armor

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/get_wither_skull=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/get_wither_skull
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/get_wither_skull=true}] run tag @e[type=armor_stand,tag=!nether-get_wither_skull-ok,name="dark_aqua"] add nether-get_wither_skull
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-get_wither_skull-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-get_wither_skull

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/obtain_blaze_rod=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/obtain_blaze_rod
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/obtain_blaze_rod=true}] run tag @e[type=armor_stand,tag=!nether-obtain_blaze_rod-ok,name="dark_aqua"] add nether-obtain_blaze_rod
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-obtain_blaze_rod-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-obtain_blaze_rod

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/charge_respawn_anchor=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/charge_respawn_anchor
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/charge_respawn_anchor=true}] run tag @e[type=armor_stand,tag=!nether-charge_respawn_anchor-ok,name="dark_aqua"] add nether-charge_respawn_anchor
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-charge_respawn_anchor-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-charge_respawn_anchor

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/ride_strider_in_overworld_lava=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/ride_strider_in_overworld_lava
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/ride_strider_in_overworld_lava=true}] run tag @e[type=armor_stand,tag=!nether-ride_strider_in_overworld_lava-ok,name="dark_aqua"] add nether-ride_strider_in_overworld_lava
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-ride_strider_in_overworld_lava-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-ride_strider_in_overworld_lava

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/explore_nether=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/explore_nether
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/explore_nether=true}] run tag @e[type=armor_stand,tag=!nether-explore_nether-ok,name="dark_aqua"] add nether-explore_nether
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-explore_nether-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-explore_nether

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/summon_wither=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/summon_wither
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/summon_wither=true}] run tag @e[type=armor_stand,tag=!nether-summon_wither-ok,name="dark_aqua"] add nether-summon_wither
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-summon_wither-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-summon_wither

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/brew_potion=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/brew_potion
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/brew_potion=true}] run tag @e[type=armor_stand,tag=!nether-brew_potion-ok,name="dark_aqua"] add nether-brew_potion
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-brew_potion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-brew_potion

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/create_beacon=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/create_beacon
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/create_beacon=true}] run tag @e[type=armor_stand,tag=!nether-create_beacon-ok,name="dark_aqua"] add nether-create_beacon
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-create_beacon-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-create_beacon

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/all_potions=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/all_potions
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/all_potions=true}] run tag @e[type=armor_stand,tag=!nether-all_potions-ok,name="dark_aqua"] add nether-all_potions
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-all_potions-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-all_potions

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/create_full_beacon=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/create_full_beacon
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/create_full_beacon=true}] run tag @e[type=armor_stand,tag=!nether-create_full_beacon-ok,name="dark_aqua"] add nether-create_full_beacon
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-create_full_beacon-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-create_full_beacon

execute if entity @a[limit=1,team=dark_aqua,advancements={nether/all_effects=true}] run advancement grant @a[team=dark_aqua] only minecraft:nether/all_effects
execute if entity @a[limit=1,team=dark_aqua,advancements={nether/all_effects=true}] run tag @e[type=armor_stand,tag=!nether-all_effects-ok,name="dark_aqua"] add nether-all_effects
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add nether-all_effects-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove nether-all_effects

execute if entity @a[limit=1,team=dark_aqua,advancements={end/root=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/root
execute if entity @a[limit=1,team=dark_aqua,advancements={end/root=true}] run tag @e[type=armor_stand,tag=!end-root-ok,name="dark_aqua"] add end-root
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-root

execute if entity @a[limit=1,team=dark_aqua,advancements={end/kill_dragon=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/kill_dragon
execute if entity @a[limit=1,team=dark_aqua,advancements={end/kill_dragon=true}] run tag @e[type=armor_stand,tag=!end-kill_dragon-ok,name="dark_aqua"] add end-kill_dragon
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-kill_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-kill_dragon

execute if entity @a[limit=1,team=dark_aqua,advancements={end/dragon_egg=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/dragon_egg
execute if entity @a[limit=1,team=dark_aqua,advancements={end/dragon_egg=true}] run tag @e[type=armor_stand,tag=!end-dragon_egg-ok,name="dark_aqua"] add end-dragon_egg
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-dragon_egg-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-dragon_egg

execute if entity @a[limit=1,team=dark_aqua,advancements={end/enter_end_gateway=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/enter_end_gateway
execute if entity @a[limit=1,team=dark_aqua,advancements={end/enter_end_gateway=true}] run tag @e[type=armor_stand,tag=!end-enter_end_gateway-ok,name="dark_aqua"] add end-enter_end_gateway
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-enter_end_gateway-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-enter_end_gateway

execute if entity @a[limit=1,team=dark_aqua,advancements={end/respawn_dragon=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/respawn_dragon
execute if entity @a[limit=1,team=dark_aqua,advancements={end/respawn_dragon=true}] run tag @e[type=armor_stand,tag=!end-respawn_dragon-ok,name="dark_aqua"] add end-respawn_dragon
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-respawn_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-respawn_dragon

execute if entity @a[limit=1,team=dark_aqua,advancements={end/dragon_breath=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/dragon_breath
execute if entity @a[limit=1,team=dark_aqua,advancements={end/dragon_breath=true}] run tag @e[type=armor_stand,tag=!end-dragon_breath-ok,name="dark_aqua"] add end-dragon_breath
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-dragon_breath-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-dragon_breath

execute if entity @a[limit=1,team=dark_aqua,advancements={end/find_end_city=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/find_end_city
execute if entity @a[limit=1,team=dark_aqua,advancements={end/find_end_city=true}] run tag @e[type=armor_stand,tag=!end-find_end_city-ok,name="dark_aqua"] add end-find_end_city
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-find_end_city-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-find_end_city

execute if entity @a[limit=1,team=dark_aqua,advancements={end/elytra=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/elytra
execute if entity @a[limit=1,team=dark_aqua,advancements={end/elytra=true}] run tag @e[type=armor_stand,tag=!end-elytra-ok,name="dark_aqua"] add end-elytra
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-elytra-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-elytra

execute if entity @a[limit=1,team=dark_aqua,advancements={end/levitate=true}] run advancement grant @a[team=dark_aqua] only minecraft:end/levitate
execute if entity @a[limit=1,team=dark_aqua,advancements={end/levitate=true}] run tag @e[type=armor_stand,tag=!end-levitate-ok,name="dark_aqua"] add end-levitate
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add end-levitate-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove end-levitate

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/root=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/root
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/root=true}] run tag @e[type=armor_stand,tag=!adventure-root-ok,name="dark_aqua"] add adventure-root
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-root

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/voluntary_exile=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/voluntary_exile
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/voluntary_exile=true}] run tag @e[type=armor_stand,tag=!adventure-voluntary_exile-ok,name="dark_aqua"] add adventure-voluntary_exile
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-voluntary_exile-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-voluntary_exile

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/spyglass_at_parrot=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/spyglass_at_parrot
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/spyglass_at_parrot=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_parrot-ok,name="dark_aqua"] add adventure-spyglass_at_parrot
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-spyglass_at_parrot-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-spyglass_at_parrot

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/kill_a_mob=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/kill_a_mob
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/kill_a_mob=true}] run tag @e[type=armor_stand,tag=!adventure-kill_a_mob-ok,name="dark_aqua"] add adventure-kill_a_mob
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-kill_a_mob-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-kill_a_mob

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/trade=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/trade
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/trade=true}] run tag @e[type=armor_stand,tag=!adventure-trade-ok,name="dark_aqua"] add adventure-trade
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-trade-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-trade

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/honey_block_slide=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/honey_block_slide
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/honey_block_slide=true}] run tag @e[type=armor_stand,tag=!adventure-honey_block_slide-ok,name="dark_aqua"] add adventure-honey_block_slide
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-honey_block_slide-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-honey_block_slide

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/ol_betsy=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/ol_betsy
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/ol_betsy=true}] run tag @e[type=armor_stand,tag=!adventure-ol_betsy-ok,name="dark_aqua"] add adventure-ol_betsy
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-ol_betsy-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-ol_betsy

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/lightning_rod_with_villager_no_fire=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/lightning_rod_with_villager_no_fire
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/lightning_rod_with_villager_no_fire=true}] run tag @e[type=armor_stand,tag=!adventure-lightning_rod_with_villager_no_fire-ok,name="dark_aqua"] add adventure-lightning_rod_with_villager_no_fire
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-lightning_rod_with_villager_no_fire-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-lightning_rod_with_villager_no_fire

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/fall_from_world_height=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/fall_from_world_height
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/fall_from_world_height=true}] run tag @e[type=armor_stand,tag=!adventure-fall_from_world_height-ok,name="dark_aqua"] add adventure-fall_from_world_height
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-fall_from_world_height-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-fall_from_world_height

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/avoid_vibration=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/avoid_vibration
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/avoid_vibration=true}] run tag @e[type=armor_stand,tag=!adventure-avoid_vibration-ok,name="dark_aqua"] add adventure-avoid_vibration
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-avoid_vibration-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-avoid_vibration

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/sleep_in_bed=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/sleep_in_bed
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/sleep_in_bed=true}] run tag @e[type=armor_stand,tag=!adventure-sleep_in_bed-ok,name="dark_aqua"] add adventure-sleep_in_bed
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-sleep_in_bed-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-sleep_in_bed

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/hero_of_the_village=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/hero_of_the_village
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/hero_of_the_village=true}] run tag @e[type=armor_stand,tag=!adventure-hero_of_the_village-ok,name="dark_aqua"] add adventure-hero_of_the_village
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-hero_of_the_village-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-hero_of_the_village

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/spyglass_at_ghast=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/spyglass_at_ghast
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/spyglass_at_ghast=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_ghast-ok,name="dark_aqua"] add adventure-spyglass_at_ghast
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-spyglass_at_ghast-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-spyglass_at_ghast

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/throw_trident=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/throw_trident
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/throw_trident=true}] run tag @e[type=armor_stand,tag=!adventure-throw_trident-ok,name="dark_aqua"] add adventure-throw_trident
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-throw_trident-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-throw_trident

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/kill_mob_near_sculk_catalyst=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/kill_mob_near_sculk_catalyst
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/kill_mob_near_sculk_catalyst=true}] run tag @e[type=armor_stand,tag=!adventure-kill_mob_near_sculk_catalyst-ok,name="dark_aqua"] add adventure-kill_mob_near_sculk_catalyst
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-kill_mob_near_sculk_catalyst-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-kill_mob_near_sculk_catalyst

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/shoot_arrow=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/shoot_arrow
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/shoot_arrow=true}] run tag @e[type=armor_stand,tag=!adventure-shoot_arrow-ok,name="dark_aqua"] add adventure-shoot_arrow
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-shoot_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-shoot_arrow

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/kill_all_mobs=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/kill_all_mobs
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/kill_all_mobs=true}] run tag @e[type=armor_stand,tag=!adventure-kill_all_mobs-ok,name="dark_aqua"] add adventure-kill_all_mobs
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-kill_all_mobs-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-kill_all_mobs

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/totem_of_undying=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/totem_of_undying
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/totem_of_undying=true}] run tag @e[type=armor_stand,tag=!adventure-totem_of_undying-ok,name="dark_aqua"] add adventure-totem_of_undying
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-totem_of_undying-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-totem_of_undying

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/summon_iron_golem=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/summon_iron_golem
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/summon_iron_golem=true}] run tag @e[type=armor_stand,tag=!adventure-summon_iron_golem-ok,name="dark_aqua"] add adventure-summon_iron_golem
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-summon_iron_golem-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-summon_iron_golem

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/trade_at_world_height=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/trade_at_world_height
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/trade_at_world_height=true}] run tag @e[type=armor_stand,tag=!adventure-trade_at_world_height-ok,name="dark_aqua"] add adventure-trade_at_world_height
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-trade_at_world_height-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-trade_at_world_height

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/two_birds_one_arrow=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/two_birds_one_arrow
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/two_birds_one_arrow=true}] run tag @e[type=armor_stand,tag=!adventure-two_birds_one_arrow-ok,name="dark_aqua"] add adventure-two_birds_one_arrow
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-two_birds_one_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-two_birds_one_arrow

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/whos_the_pillager_now=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/whos_the_pillager_now
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/whos_the_pillager_now=true}] run tag @e[type=armor_stand,tag=!adventure-whos_the_pillager_now-ok,name="dark_aqua"] add adventure-whos_the_pillager_now
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-whos_the_pillager_now-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-whos_the_pillager_now

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/arbalistic=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/arbalistic
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/arbalistic=true}] run tag @e[type=armor_stand,tag=!adventure-arbalistic-ok,name="dark_aqua"] add adventure-arbalistic
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-arbalistic-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-arbalistic

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/adventuring_time=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/adventuring_time
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/adventuring_time=true}] run tag @e[type=armor_stand,tag=!adventure-adventuring_time-ok,name="dark_aqua"] add adventure-adventuring_time
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-adventuring_time-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-adventuring_time

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/play_jukebox_in_meadows=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/play_jukebox_in_meadows
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/play_jukebox_in_meadows=true}] run tag @e[type=armor_stand,tag=!adventure-play_jukebox_in_meadows-ok,name="dark_aqua"] add adventure-play_jukebox_in_meadows
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-play_jukebox_in_meadows-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-play_jukebox_in_meadows

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/walk_on_powder_snow_with_leather_boots=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/walk_on_powder_snow_with_leather_boots
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/walk_on_powder_snow_with_leather_boots=true}] run tag @e[type=armor_stand,tag=!adventure-walk_on_powder_snow_with_leather_boots-ok,name="dark_aqua"] add adventure-walk_on_powder_snow_with_leather_boots
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-walk_on_powder_snow_with_leather_boots-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-walk_on_powder_snow_with_leather_boots

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/spyglass_at_dragon=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/spyglass_at_dragon
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/spyglass_at_dragon=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_dragon-ok,name="dark_aqua"] add adventure-spyglass_at_dragon
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-spyglass_at_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-spyglass_at_dragon

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/very_very_frightening=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/very_very_frightening
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/very_very_frightening=true}] run tag @e[type=armor_stand,tag=!adventure-very_very_frightening-ok,name="dark_aqua"] add adventure-very_very_frightening
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-very_very_frightening-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-very_very_frightening

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/sniper_duel=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/sniper_duel
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/sniper_duel=true}] run tag @e[type=armor_stand,tag=!adventure-sniper_duel-ok,name="dark_aqua"] add adventure-sniper_duel
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-sniper_duel-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-sniper_duel

execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/bullseye=true}] run advancement grant @a[team=dark_aqua] only minecraft:adventure/bullseye
execute if entity @a[limit=1,team=dark_aqua,advancements={adventure/bullseye=true}] run tag @e[type=armor_stand,tag=!adventure-bullseye-ok,name="dark_aqua"] add adventure-bullseye
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add adventure-bullseye-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove adventure-bullseye

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/root=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/root
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/root=true}] run tag @e[type=armor_stand,tag=!husbandry-root-ok,name="dark_aqua"] add husbandry-root
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-root

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/safely_harvest_honey=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/safely_harvest_honey
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/safely_harvest_honey=true}] run tag @e[type=armor_stand,tag=!husbandry-safely_harvest_honey-ok,name="dark_aqua"] add husbandry-safely_harvest_honey
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-safely_harvest_honey-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-safely_harvest_honey

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/breed_an_animal=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/breed_an_animal
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/breed_an_animal=true}] run tag @e[type=armor_stand,tag=!husbandry-breed_an_animal-ok,name="dark_aqua"] add husbandry-breed_an_animal
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-breed_an_animal-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-breed_an_animal

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/allay_deliver_item_to_player=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/allay_deliver_item_to_player
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/allay_deliver_item_to_player=true}] run tag @e[type=armor_stand,tag=!husbandry-allay_deliver_item_to_player-ok,name="dark_aqua"] add husbandry-allay_deliver_item_to_player
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-allay_deliver_item_to_player-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-allay_deliver_item_to_player

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/ride_a_boat_with_a_goat=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/ride_a_boat_with_a_goat
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/ride_a_boat_with_a_goat=true}] run tag @e[type=armor_stand,tag=!husbandry-ride_a_boat_with_a_goat-ok,name="dark_aqua"] add husbandry-ride_a_boat_with_a_goat
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-ride_a_boat_with_a_goat-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-ride_a_boat_with_a_goat

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/tame_an_animal=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/tame_an_animal
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/tame_an_animal=true}] run tag @e[type=armor_stand,tag=!husbandry-tame_an_animal-ok,name="dark_aqua"] add husbandry-tame_an_animal
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-tame_an_animal-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-tame_an_animal

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/make_a_sign_glow=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/make_a_sign_glow
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/make_a_sign_glow=true}] run tag @e[type=armor_stand,tag=!husbandry-make_a_sign_glow-ok,name="dark_aqua"] add husbandry-make_a_sign_glow
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-make_a_sign_glow-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-make_a_sign_glow

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/fishy_business=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/fishy_business
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/fishy_business=true}] run tag @e[type=armor_stand,tag=!husbandry-fishy_business-ok,name="dark_aqua"] add husbandry-fishy_business
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-fishy_business-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-fishy_business

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/silk_touch_nest=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/silk_touch_nest
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/silk_touch_nest=true}] run tag @e[type=armor_stand,tag=!husbandry-silk_touch_nest-ok,name="dark_aqua"] add husbandry-silk_touch_nest
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-silk_touch_nest-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-silk_touch_nest

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/tadpole_in_a_bucket=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/tadpole_in_a_bucket
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/tadpole_in_a_bucket=true}] run tag @e[type=armor_stand,tag=!husbandry-tadpole_in_a_bucket-ok,name="dark_aqua"] add husbandry-tadpole_in_a_bucket
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-tadpole_in_a_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-tadpole_in_a_bucket

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/plant_seed=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/plant_seed
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/plant_seed=true}] run tag @e[type=armor_stand,tag=!husbandry-plant_seed-ok,name="dark_aqua"] add husbandry-plant_seed
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-plant_seed-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-plant_seed

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/wax_on=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/wax_on
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/wax_on=true}] run tag @e[type=armor_stand,tag=!husbandry-wax_on-ok,name="dark_aqua"] add husbandry-wax_on
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-wax_on-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-wax_on

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/bred_all_animals=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/bred_all_animals
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/bred_all_animals=true}] run tag @e[type=armor_stand,tag=!husbandry-bred_all_animals-ok,name="dark_aqua"] add husbandry-bred_all_animals
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-bred_all_animals-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-bred_all_animals

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/allay_deliver_cake_to_note_block=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/allay_deliver_cake_to_note_block
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/allay_deliver_cake_to_note_block=true}] run tag @e[type=armor_stand,tag=!husbandry-allay_deliver_cake_to_note_block-ok,name="dark_aqua"] add husbandry-allay_deliver_cake_to_note_block
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-allay_deliver_cake_to_note_block-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-allay_deliver_cake_to_note_block

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/complete_catalogue=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/complete_catalogue
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/complete_catalogue=true}] run tag @e[type=armor_stand,tag=!husbandry-complete_catalogue-ok,name="dark_aqua"] add husbandry-complete_catalogue
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-complete_catalogue-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-complete_catalogue

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/tactical_fishing=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/tactical_fishing
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/tactical_fishing=true}] run tag @e[type=armor_stand,tag=!husbandry-tactical_fishing-ok,name="dark_aqua"] add husbandry-tactical_fishing
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-tactical_fishing-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-tactical_fishing

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/leash_all_frog_variants=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/leash_all_frog_variants
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/leash_all_frog_variants=true}] run tag @e[type=armor_stand,tag=!husbandry-leash_all_frog_variants-ok,name="dark_aqua"] add husbandry-leash_all_frog_variants
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-leash_all_frog_variants-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-leash_all_frog_variants

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/balanced_diet=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/balanced_diet
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/balanced_diet=true}] run tag @e[type=armor_stand,tag=!husbandry-balanced_diet-ok,name="dark_aqua"] add husbandry-balanced_diet
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-balanced_diet-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-balanced_diet

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/obtain_netherite_hoe=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/obtain_netherite_hoe
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/obtain_netherite_hoe=true}] run tag @e[type=armor_stand,tag=!husbandry-obtain_netherite_hoe-ok,name="dark_aqua"] add husbandry-obtain_netherite_hoe
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-obtain_netherite_hoe-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-obtain_netherite_hoe

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/wax_off=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/wax_off
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/wax_off=true}] run tag @e[type=armor_stand,tag=!husbandry-wax_off-ok,name="dark_aqua"] add husbandry-wax_off
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-wax_off-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-wax_off

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/axolotl_in_a_bucket=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/axolotl_in_a_bucket
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/axolotl_in_a_bucket=true}] run tag @e[type=armor_stand,tag=!husbandry-axolotl_in_a_bucket-ok,name="dark_aqua"] add husbandry-axolotl_in_a_bucket
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-axolotl_in_a_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-axolotl_in_a_bucket

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/froglights=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/froglights
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/froglights=true}] run tag @e[type=armor_stand,tag=!husbandry-froglights-ok,name="dark_aqua"] add husbandry-froglights
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-froglights-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-froglights

execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/kill_axolotl_target=true}] run advancement grant @a[team=dark_aqua] only minecraft:husbandry/kill_axolotl_target
execute if entity @a[limit=1,team=dark_aqua,advancements={husbandry/kill_axolotl_target=true}] run tag @e[type=armor_stand,tag=!husbandry-kill_axolotl_target-ok,name="dark_aqua"] add husbandry-kill_axolotl_target
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="dark_aqua"] run scoreboard players add dark_aqua Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] add husbandry-kill_axolotl_target-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="dark_aqua"] run tag @e[type=armor_stand,name="dark_aqua"] remove husbandry-kill_axolotl_target

schedule function bingo:dark_aqua 1s