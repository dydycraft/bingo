execute if entity @a[limit=1,team=red,advancements={story/root=true}] run advancement grant @a[team=red] only minecraft:story/root
execute if entity @a[limit=1,team=red,advancements={story/root=true}] run tag @e[type=armor_stand,tag=!story-root-ok,name="red"] add story-root
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="red"] run tag @e[type=armor_stand,name="red"] add story-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-root,name="red"] run tag @e[type=armor_stand,name="red"] remove story-root

execute if entity @a[limit=1,team=red,advancements={story/mine_stone=true}] run advancement grant @a[team=red] only minecraft:story/mine_stone
execute if entity @a[limit=1,team=red,advancements={story/mine_stone=true}] run tag @e[type=armor_stand,tag=!story-mine_stone-ok,name="red"] add story-mine_stone
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="red"] run tag @e[type=armor_stand,name="red"] add story-mine_stone-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_stone,name="red"] run tag @e[type=armor_stand,name="red"] remove story-mine_stone

execute if entity @a[limit=1,team=red,advancements={story/upgrade_tools=true}] run advancement grant @a[team=red] only minecraft:story/upgrade_tools
execute if entity @a[limit=1,team=red,advancements={story/upgrade_tools=true}] run tag @e[type=armor_stand,tag=!story-upgrade_tools-ok,name="red"] add story-upgrade_tools
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="red"] run tag @e[type=armor_stand,name="red"] add story-upgrade_tools-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-upgrade_tools,name="red"] run tag @e[type=armor_stand,name="red"] remove story-upgrade_tools

execute if entity @a[limit=1,team=red,advancements={story/smelt_iron=true}] run advancement grant @a[team=red] only minecraft:story/smelt_iron
execute if entity @a[limit=1,team=red,advancements={story/smelt_iron=true}] run tag @e[type=armor_stand,tag=!story-smelt_iron-ok,name="red"] add story-smelt_iron
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="red"] run tag @e[type=armor_stand,name="red"] add story-smelt_iron-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-smelt_iron,name="red"] run tag @e[type=armor_stand,name="red"] remove story-smelt_iron

execute if entity @a[limit=1,team=red,advancements={story/obtain_armor=true}] run advancement grant @a[team=red] only minecraft:story/obtain_armor
execute if entity @a[limit=1,team=red,advancements={story/obtain_armor=true}] run tag @e[type=armor_stand,tag=!story-obtain_armor-ok,name="red"] add story-obtain_armor
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="red"] run tag @e[type=armor_stand,name="red"] add story-obtain_armor-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-obtain_armor,name="red"] run tag @e[type=armor_stand,name="red"] remove story-obtain_armor

execute if entity @a[limit=1,team=red,advancements={story/lava_bucket=true}] run advancement grant @a[team=red] only minecraft:story/lava_bucket
execute if entity @a[limit=1,team=red,advancements={story/lava_bucket=true}] run tag @e[type=armor_stand,tag=!story-lava_bucket-ok,name="red"] add story-lava_bucket
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="red"] run tag @e[type=armor_stand,name="red"] add story-lava_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-lava_bucket,name="red"] run tag @e[type=armor_stand,name="red"] remove story-lava_bucket

execute if entity @a[limit=1,team=red,advancements={story/iron_tools=true}] run advancement grant @a[team=red] only minecraft:story/iron_tools
execute if entity @a[limit=1,team=red,advancements={story/iron_tools=true}] run tag @e[type=armor_stand,tag=!story-iron_tools-ok,name="red"] add story-iron_tools
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="red"] run tag @e[type=armor_stand,name="red"] add story-iron_tools-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-iron_tools,name="red"] run tag @e[type=armor_stand,name="red"] remove story-iron_tools

execute if entity @a[limit=1,team=red,advancements={story/deflect_arrow=true}] run advancement grant @a[team=red] only minecraft:story/deflect_arrow
execute if entity @a[limit=1,team=red,advancements={story/deflect_arrow=true}] run tag @e[type=armor_stand,tag=!story-deflect_arrow-ok,name="red"] add story-deflect_arrow
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="red"] run tag @e[type=armor_stand,name="red"] add story-deflect_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-deflect_arrow,name="red"] run tag @e[type=armor_stand,name="red"] remove story-deflect_arrow

execute if entity @a[limit=1,team=red,advancements={story/form_obsidian=true}] run advancement grant @a[team=red] only minecraft:story/form_obsidian
execute if entity @a[limit=1,team=red,advancements={story/form_obsidian=true}] run tag @e[type=armor_stand,tag=!story-form_obsidian-ok,name="red"] add story-form_obsidian
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="red"] run tag @e[type=armor_stand,name="red"] add story-form_obsidian-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-form_obsidian,name="red"] run tag @e[type=armor_stand,name="red"] remove story-form_obsidian

execute if entity @a[limit=1,team=red,advancements={story/mine_diamond=true}] run advancement grant @a[team=red] only minecraft:story/mine_diamond
execute if entity @a[limit=1,team=red,advancements={story/mine_diamond=true}] run tag @e[type=armor_stand,tag=!story-mine_diamond-ok,name="red"] add story-mine_diamond
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="red"] run tag @e[type=armor_stand,name="red"] add story-mine_diamond-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-mine_diamond,name="red"] run tag @e[type=armor_stand,name="red"] remove story-mine_diamond

execute if entity @a[limit=1,team=red,advancements={story/enter_the_nether=true}] run advancement grant @a[team=red] only minecraft:story/enter_the_nether
execute if entity @a[limit=1,team=red,advancements={story/enter_the_nether=true}] run tag @e[type=armor_stand,tag=!story-enter_the_nether-ok,name="red"] add story-enter_the_nether
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="red"] run tag @e[type=armor_stand,name="red"] add story-enter_the_nether-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_nether,name="red"] run tag @e[type=armor_stand,name="red"] remove story-enter_the_nether

execute if entity @a[limit=1,team=red,advancements={story/shiny_gear=true}] run advancement grant @a[team=red] only minecraft:story/shiny_gear
execute if entity @a[limit=1,team=red,advancements={story/shiny_gear=true}] run tag @e[type=armor_stand,tag=!story-shiny_gear-ok,name="red"] add story-shiny_gear
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="red"] run tag @e[type=armor_stand,name="red"] add story-shiny_gear-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-shiny_gear,name="red"] run tag @e[type=armor_stand,name="red"] remove story-shiny_gear

execute if entity @a[limit=1,team=red,advancements={story/enchant_item=true}] run advancement grant @a[team=red] only minecraft:story/enchant_item
execute if entity @a[limit=1,team=red,advancements={story/enchant_item=true}] run tag @e[type=armor_stand,tag=!story-enchant_item-ok,name="red"] add story-enchant_item
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="red"] run tag @e[type=armor_stand,name="red"] add story-enchant_item-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enchant_item,name="red"] run tag @e[type=armor_stand,name="red"] remove story-enchant_item

execute if entity @a[limit=1,team=red,advancements={story/cure_zombie_villager=true}] run advancement grant @a[team=red] only minecraft:story/cure_zombie_villager
execute if entity @a[limit=1,team=red,advancements={story/cure_zombie_villager=true}] run tag @e[type=armor_stand,tag=!story-cure_zombie_villager-ok,name="red"] add story-cure_zombie_villager
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="red"] run tag @e[type=armor_stand,name="red"] add story-cure_zombie_villager-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-cure_zombie_villager,name="red"] run tag @e[type=armor_stand,name="red"] remove story-cure_zombie_villager

execute if entity @a[limit=1,team=red,advancements={story/follow_ender_eye=true}] run advancement grant @a[team=red] only minecraft:story/follow_ender_eye
execute if entity @a[limit=1,team=red,advancements={story/follow_ender_eye=true}] run tag @e[type=armor_stand,tag=!story-follow_ender_eye-ok,name="red"] add story-follow_ender_eye
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="red"] run tag @e[type=armor_stand,name="red"] add story-follow_ender_eye-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-follow_ender_eye,name="red"] run tag @e[type=armor_stand,name="red"] remove story-follow_ender_eye

execute if entity @a[limit=1,team=red,advancements={story/enter_the_end=true}] run advancement grant @a[team=red] only minecraft:story/enter_the_end
execute if entity @a[limit=1,team=red,advancements={story/enter_the_end=true}] run tag @e[type=armor_stand,tag=!story-enter_the_end-ok,name="red"] add story-enter_the_end
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="red"] run tag @e[type=armor_stand,name="red"] add story-enter_the_end-ok
execute if entity @e[type=armor_stand,limit=1,tag=story-enter_the_end,name="red"] run tag @e[type=armor_stand,name="red"] remove story-enter_the_end

execute if entity @a[limit=1,team=red,advancements={nether/root=true}] run advancement grant @a[team=red] only minecraft:nether/root
execute if entity @a[limit=1,team=red,advancements={nether/root=true}] run tag @e[type=armor_stand,tag=!nether-root-ok,name="red"] add nether-root
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="red"] run tag @e[type=armor_stand,name="red"] add nether-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-root,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-root

execute if entity @a[limit=1,team=red,advancements={nether/return_to_sender=true}] run advancement grant @a[team=red] only minecraft:nether/return_to_sender
execute if entity @a[limit=1,team=red,advancements={nether/return_to_sender=true}] run tag @e[type=armor_stand,tag=!nether-return_to_sender-ok,name="red"] add nether-return_to_sender
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="red"] run tag @e[type=armor_stand,name="red"] add nether-return_to_sender-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-return_to_sender,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-return_to_sender

execute if entity @a[limit=1,team=red,advancements={nether/find_bastion=true}] run advancement grant @a[team=red] only minecraft:nether/find_bastion
execute if entity @a[limit=1,team=red,advancements={nether/find_bastion=true}] run tag @e[type=armor_stand,tag=!nether-find_bastion-ok,name="red"] add nether-find_bastion
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="red"] run tag @e[type=armor_stand,name="red"] add nether-find_bastion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_bastion,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-find_bastion

execute if entity @a[limit=1,team=red,advancements={nether/obtain_ancient_debris=true}] run advancement grant @a[team=red] only minecraft:nether/obtain_ancient_debris
execute if entity @a[limit=1,team=red,advancements={nether/obtain_ancient_debris=true}] run tag @e[type=armor_stand,tag=!nether-obtain_ancient_debris-ok,name="red"] add nether-obtain_ancient_debris
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="red"] run tag @e[type=armor_stand,name="red"] add nether-obtain_ancient_debris-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_ancient_debris,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-obtain_ancient_debris

execute if entity @a[limit=1,team=red,advancements={nether/fast_travel=true}] run advancement grant @a[team=red] only minecraft:nether/fast_travel
execute if entity @a[limit=1,team=red,advancements={nether/fast_travel=true}] run tag @e[type=armor_stand,tag=!nether-fast_travel-ok,name="red"] add nether-fast_travel
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="red"] run tag @e[type=armor_stand,name="red"] add nether-fast_travel-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-fast_travel,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-fast_travel

execute if entity @a[limit=1,team=red,advancements={nether/find_fortress=true}] run advancement grant @a[team=red] only minecraft:nether/find_fortress
execute if entity @a[limit=1,team=red,advancements={nether/find_fortress=true}] run tag @e[type=armor_stand,tag=!nether-find_fortress-ok,name="red"] add nether-find_fortress
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="red"] run tag @e[type=armor_stand,name="red"] add nether-find_fortress-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-find_fortress,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-find_fortress

execute if entity @a[limit=1,team=red,advancements={nether/obtain_crying_obsidian=true}] run advancement grant @a[team=red] only minecraft:nether/obtain_crying_obsidian
execute if entity @a[limit=1,team=red,advancements={nether/obtain_crying_obsidian=true}] run tag @e[type=armor_stand,tag=!nether-obtain_crying_obsidian-ok,name="red"] add nether-obtain_crying_obsidian
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="red"] run tag @e[type=armor_stand,name="red"] add nether-obtain_crying_obsidian-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_crying_obsidian,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-obtain_crying_obsidian

execute if entity @a[limit=1,team=red,advancements={nether/distract_piglin=true}] run advancement grant @a[team=red] only minecraft:nether/distract_piglin
execute if entity @a[limit=1,team=red,advancements={nether/distract_piglin=true}] run tag @e[type=armor_stand,tag=!nether-distract_piglin-ok,name="red"] add nether-distract_piglin
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="red"] run tag @e[type=armor_stand,name="red"] add nether-distract_piglin-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-distract_piglin,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-distract_piglin

execute if entity @a[limit=1,team=red,advancements={nether/ride_strider=true}] run advancement grant @a[team=red] only minecraft:nether/ride_strider
execute if entity @a[limit=1,team=red,advancements={nether/ride_strider=true}] run tag @e[type=armor_stand,tag=!nether-ride_strider-ok,name="red"] add nether-ride_strider
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="red"] run tag @e[type=armor_stand,name="red"] add nether-ride_strider-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-ride_strider

execute if entity @a[limit=1,team=red,advancements={nether/uneasy_alliance=true}] run advancement grant @a[team=red] only minecraft:nether/uneasy_alliance
execute if entity @a[limit=1,team=red,advancements={nether/uneasy_alliance=true}] run tag @e[type=armor_stand,tag=!nether-uneasy_alliance-ok,name="red"] add nether-uneasy_alliance
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="red"] run tag @e[type=armor_stand,name="red"] add nether-uneasy_alliance-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-uneasy_alliance,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-uneasy_alliance

execute if entity @a[limit=1,team=red,advancements={nether/loot_bastion=true}] run advancement grant @a[team=red] only minecraft:nether/loot_bastion
execute if entity @a[limit=1,team=red,advancements={nether/loot_bastion=true}] run tag @e[type=armor_stand,tag=!nether-loot_bastion-ok,name="red"] add nether-loot_bastion
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="red"] run tag @e[type=armor_stand,name="red"] add nether-loot_bastion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-loot_bastion,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-loot_bastion

execute if entity @a[limit=1,team=red,advancements={nether/use_lodestone=true}] run advancement grant @a[team=red] only minecraft:nether/use_lodestone
execute if entity @a[limit=1,team=red,advancements={nether/use_lodestone=true}] run tag @e[type=armor_stand,tag=!nether-use_lodestone-ok,name="red"] add nether-use_lodestone
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="red"] run tag @e[type=armor_stand,name="red"] add nether-use_lodestone-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-use_lodestone,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-use_lodestone

execute if entity @a[limit=1,team=red,advancements={nether/netherite_armor=true}] run advancement grant @a[team=red] only minecraft:nether/netherite_armor
execute if entity @a[limit=1,team=red,advancements={nether/netherite_armor=true}] run tag @e[type=armor_stand,tag=!nether-netherite_armor-ok,name="red"] add nether-netherite_armor
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="red"] run tag @e[type=armor_stand,name="red"] add nether-netherite_armor-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-netherite_armor,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-netherite_armor

execute if entity @a[limit=1,team=red,advancements={nether/get_wither_skull=true}] run advancement grant @a[team=red] only minecraft:nether/get_wither_skull
execute if entity @a[limit=1,team=red,advancements={nether/get_wither_skull=true}] run tag @e[type=armor_stand,tag=!nether-get_wither_skull-ok,name="red"] add nether-get_wither_skull
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="red"] run tag @e[type=armor_stand,name="red"] add nether-get_wither_skull-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-get_wither_skull,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-get_wither_skull

execute if entity @a[limit=1,team=red,advancements={nether/obtain_blaze_rod=true}] run advancement grant @a[team=red] only minecraft:nether/obtain_blaze_rod
execute if entity @a[limit=1,team=red,advancements={nether/obtain_blaze_rod=true}] run tag @e[type=armor_stand,tag=!nether-obtain_blaze_rod-ok,name="red"] add nether-obtain_blaze_rod
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="red"] run tag @e[type=armor_stand,name="red"] add nether-obtain_blaze_rod-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-obtain_blaze_rod,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-obtain_blaze_rod

execute if entity @a[limit=1,team=red,advancements={nether/charge_respawn_anchor=true}] run advancement grant @a[team=red] only minecraft:nether/charge_respawn_anchor
execute if entity @a[limit=1,team=red,advancements={nether/charge_respawn_anchor=true}] run tag @e[type=armor_stand,tag=!nether-charge_respawn_anchor-ok,name="red"] add nether-charge_respawn_anchor
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="red"] run tag @e[type=armor_stand,name="red"] add nether-charge_respawn_anchor-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-charge_respawn_anchor,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-charge_respawn_anchor

execute if entity @a[limit=1,team=red,advancements={nether/ride_strider_in_overworld_lava=true}] run advancement grant @a[team=red] only minecraft:nether/ride_strider_in_overworld_lava
execute if entity @a[limit=1,team=red,advancements={nether/ride_strider_in_overworld_lava=true}] run tag @e[type=armor_stand,tag=!nether-ride_strider_in_overworld_lava-ok,name="red"] add nether-ride_strider_in_overworld_lava
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="red"] run tag @e[type=armor_stand,name="red"] add nether-ride_strider_in_overworld_lava-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-ride_strider_in_overworld_lava,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-ride_strider_in_overworld_lava

execute if entity @a[limit=1,team=red,advancements={nether/explore_nether=true}] run advancement grant @a[team=red] only minecraft:nether/explore_nether
execute if entity @a[limit=1,team=red,advancements={nether/explore_nether=true}] run tag @e[type=armor_stand,tag=!nether-explore_nether-ok,name="red"] add nether-explore_nether
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="red"] run tag @e[type=armor_stand,name="red"] add nether-explore_nether-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-explore_nether,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-explore_nether

execute if entity @a[limit=1,team=red,advancements={nether/summon_wither=true}] run advancement grant @a[team=red] only minecraft:nether/summon_wither
execute if entity @a[limit=1,team=red,advancements={nether/summon_wither=true}] run tag @e[type=armor_stand,tag=!nether-summon_wither-ok,name="red"] add nether-summon_wither
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="red"] run tag @e[type=armor_stand,name="red"] add nether-summon_wither-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-summon_wither,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-summon_wither

execute if entity @a[limit=1,team=red,advancements={nether/brew_potion=true}] run advancement grant @a[team=red] only minecraft:nether/brew_potion
execute if entity @a[limit=1,team=red,advancements={nether/brew_potion=true}] run tag @e[type=armor_stand,tag=!nether-brew_potion-ok,name="red"] add nether-brew_potion
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="red"] run tag @e[type=armor_stand,name="red"] add nether-brew_potion-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-brew_potion,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-brew_potion

execute if entity @a[limit=1,team=red,advancements={nether/create_beacon=true}] run advancement grant @a[team=red] only minecraft:nether/create_beacon
execute if entity @a[limit=1,team=red,advancements={nether/create_beacon=true}] run tag @e[type=armor_stand,tag=!nether-create_beacon-ok,name="red"] add nether-create_beacon
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="red"] run tag @e[type=armor_stand,name="red"] add nether-create_beacon-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_beacon,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-create_beacon

execute if entity @a[limit=1,team=red,advancements={nether/all_potions=true}] run advancement grant @a[team=red] only minecraft:nether/all_potions
execute if entity @a[limit=1,team=red,advancements={nether/all_potions=true}] run tag @e[type=armor_stand,tag=!nether-all_potions-ok,name="red"] add nether-all_potions
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="red"] run tag @e[type=armor_stand,name="red"] add nether-all_potions-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_potions,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-all_potions

execute if entity @a[limit=1,team=red,advancements={nether/create_full_beacon=true}] run advancement grant @a[team=red] only minecraft:nether/create_full_beacon
execute if entity @a[limit=1,team=red,advancements={nether/create_full_beacon=true}] run tag @e[type=armor_stand,tag=!nether-create_full_beacon-ok,name="red"] add nether-create_full_beacon
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="red"] run tag @e[type=armor_stand,name="red"] add nether-create_full_beacon-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-create_full_beacon,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-create_full_beacon

execute if entity @a[limit=1,team=red,advancements={nether/all_effects=true}] run advancement grant @a[team=red] only minecraft:nether/all_effects
execute if entity @a[limit=1,team=red,advancements={nether/all_effects=true}] run tag @e[type=armor_stand,tag=!nether-all_effects-ok,name="red"] add nether-all_effects
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="red"] run tag @e[type=armor_stand,name="red"] add nether-all_effects-ok
execute if entity @e[type=armor_stand,limit=1,tag=nether-all_effects,name="red"] run tag @e[type=armor_stand,name="red"] remove nether-all_effects

execute if entity @a[limit=1,team=red,advancements={end/root=true}] run advancement grant @a[team=red] only minecraft:end/root
execute if entity @a[limit=1,team=red,advancements={end/root=true}] run tag @e[type=armor_stand,tag=!end-root-ok,name="red"] add end-root
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="red"] run tag @e[type=armor_stand,name="red"] add end-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-root,name="red"] run tag @e[type=armor_stand,name="red"] remove end-root

execute if entity @a[limit=1,team=red,advancements={end/kill_dragon=true}] run advancement grant @a[team=red] only minecraft:end/kill_dragon
execute if entity @a[limit=1,team=red,advancements={end/kill_dragon=true}] run tag @e[type=armor_stand,tag=!end-kill_dragon-ok,name="red"] add end-kill_dragon
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="red"] run tag @e[type=armor_stand,name="red"] add end-kill_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-kill_dragon,name="red"] run tag @e[type=armor_stand,name="red"] remove end-kill_dragon

execute if entity @a[limit=1,team=red,advancements={end/dragon_egg=true}] run advancement grant @a[team=red] only minecraft:end/dragon_egg
execute if entity @a[limit=1,team=red,advancements={end/dragon_egg=true}] run tag @e[type=armor_stand,tag=!end-dragon_egg-ok,name="red"] add end-dragon_egg
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="red"] run tag @e[type=armor_stand,name="red"] add end-dragon_egg-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_egg,name="red"] run tag @e[type=armor_stand,name="red"] remove end-dragon_egg

execute if entity @a[limit=1,team=red,advancements={end/enter_end_gateway=true}] run advancement grant @a[team=red] only minecraft:end/enter_end_gateway
execute if entity @a[limit=1,team=red,advancements={end/enter_end_gateway=true}] run tag @e[type=armor_stand,tag=!end-enter_end_gateway-ok,name="red"] add end-enter_end_gateway
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="red"] run tag @e[type=armor_stand,name="red"] add end-enter_end_gateway-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-enter_end_gateway,name="red"] run tag @e[type=armor_stand,name="red"] remove end-enter_end_gateway

execute if entity @a[limit=1,team=red,advancements={end/respawn_dragon=true}] run advancement grant @a[team=red] only minecraft:end/respawn_dragon
execute if entity @a[limit=1,team=red,advancements={end/respawn_dragon=true}] run tag @e[type=armor_stand,tag=!end-respawn_dragon-ok,name="red"] add end-respawn_dragon
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="red"] run tag @e[type=armor_stand,name="red"] add end-respawn_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-respawn_dragon,name="red"] run tag @e[type=armor_stand,name="red"] remove end-respawn_dragon

execute if entity @a[limit=1,team=red,advancements={end/dragon_breath=true}] run advancement grant @a[team=red] only minecraft:end/dragon_breath
execute if entity @a[limit=1,team=red,advancements={end/dragon_breath=true}] run tag @e[type=armor_stand,tag=!end-dragon_breath-ok,name="red"] add end-dragon_breath
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="red"] run tag @e[type=armor_stand,name="red"] add end-dragon_breath-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-dragon_breath,name="red"] run tag @e[type=armor_stand,name="red"] remove end-dragon_breath

execute if entity @a[limit=1,team=red,advancements={end/find_end_city=true}] run advancement grant @a[team=red] only minecraft:end/find_end_city
execute if entity @a[limit=1,team=red,advancements={end/find_end_city=true}] run tag @e[type=armor_stand,tag=!end-find_end_city-ok,name="red"] add end-find_end_city
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="red"] run tag @e[type=armor_stand,name="red"] add end-find_end_city-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-find_end_city,name="red"] run tag @e[type=armor_stand,name="red"] remove end-find_end_city

execute if entity @a[limit=1,team=red,advancements={end/elytra=true}] run advancement grant @a[team=red] only minecraft:end/elytra
execute if entity @a[limit=1,team=red,advancements={end/elytra=true}] run tag @e[type=armor_stand,tag=!end-elytra-ok,name="red"] add end-elytra
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="red"] run tag @e[type=armor_stand,name="red"] add end-elytra-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-elytra,name="red"] run tag @e[type=armor_stand,name="red"] remove end-elytra

execute if entity @a[limit=1,team=red,advancements={end/levitate=true}] run advancement grant @a[team=red] only minecraft:end/levitate
execute if entity @a[limit=1,team=red,advancements={end/levitate=true}] run tag @e[type=armor_stand,tag=!end-levitate-ok,name="red"] add end-levitate
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="red"] run tag @e[type=armor_stand,name="red"] add end-levitate-ok
execute if entity @e[type=armor_stand,limit=1,tag=end-levitate,name="red"] run tag @e[type=armor_stand,name="red"] remove end-levitate

execute if entity @a[limit=1,team=red,advancements={adventure/root=true}] run advancement grant @a[team=red] only minecraft:adventure/root
execute if entity @a[limit=1,team=red,advancements={adventure/root=true}] run tag @e[type=armor_stand,tag=!adventure-root-ok,name="red"] add adventure-root
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-root,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-root

execute if entity @a[limit=1,team=red,advancements={adventure/voluntary_exile=true}] run advancement grant @a[team=red] only minecraft:adventure/voluntary_exile
execute if entity @a[limit=1,team=red,advancements={adventure/voluntary_exile=true}] run tag @e[type=armor_stand,tag=!adventure-voluntary_exile-ok,name="red"] add adventure-voluntary_exile
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-voluntary_exile-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-voluntary_exile,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-voluntary_exile

execute if entity @a[limit=1,team=red,advancements={adventure/spyglass_at_parrot=true}] run advancement grant @a[team=red] only minecraft:adventure/spyglass_at_parrot
execute if entity @a[limit=1,team=red,advancements={adventure/spyglass_at_parrot=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_parrot-ok,name="red"] add adventure-spyglass_at_parrot
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-spyglass_at_parrot-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_parrot,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-spyglass_at_parrot

execute if entity @a[limit=1,team=red,advancements={adventure/kill_a_mob=true}] run advancement grant @a[team=red] only minecraft:adventure/kill_a_mob
execute if entity @a[limit=1,team=red,advancements={adventure/kill_a_mob=true}] run tag @e[type=armor_stand,tag=!adventure-kill_a_mob-ok,name="red"] add adventure-kill_a_mob
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-kill_a_mob-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_a_mob,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-kill_a_mob

execute if entity @a[limit=1,team=red,advancements={adventure/trade=true}] run advancement grant @a[team=red] only minecraft:adventure/trade
execute if entity @a[limit=1,team=red,advancements={adventure/trade=true}] run tag @e[type=armor_stand,tag=!adventure-trade-ok,name="red"] add adventure-trade
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-trade-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-trade

execute if entity @a[limit=1,team=red,advancements={adventure/honey_block_slide=true}] run advancement grant @a[team=red] only minecraft:adventure/honey_block_slide
execute if entity @a[limit=1,team=red,advancements={adventure/honey_block_slide=true}] run tag @e[type=armor_stand,tag=!adventure-honey_block_slide-ok,name="red"] add adventure-honey_block_slide
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-honey_block_slide-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-honey_block_slide,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-honey_block_slide

execute if entity @a[limit=1,team=red,advancements={adventure/ol_betsy=true}] run advancement grant @a[team=red] only minecraft:adventure/ol_betsy
execute if entity @a[limit=1,team=red,advancements={adventure/ol_betsy=true}] run tag @e[type=armor_stand,tag=!adventure-ol_betsy-ok,name="red"] add adventure-ol_betsy
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-ol_betsy-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-ol_betsy,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-ol_betsy

execute if entity @a[limit=1,team=red,advancements={adventure/lightning_rod_with_villager_no_fire=true}] run advancement grant @a[team=red] only minecraft:adventure/lightning_rod_with_villager_no_fire
execute if entity @a[limit=1,team=red,advancements={adventure/lightning_rod_with_villager_no_fire=true}] run tag @e[type=armor_stand,tag=!adventure-lightning_rod_with_villager_no_fire-ok,name="red"] add adventure-lightning_rod_with_villager_no_fire
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-lightning_rod_with_villager_no_fire-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-lightning_rod_with_villager_no_fire,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-lightning_rod_with_villager_no_fire

execute if entity @a[limit=1,team=red,advancements={adventure/fall_from_world_height=true}] run advancement grant @a[team=red] only minecraft:adventure/fall_from_world_height
execute if entity @a[limit=1,team=red,advancements={adventure/fall_from_world_height=true}] run tag @e[type=armor_stand,tag=!adventure-fall_from_world_height-ok,name="red"] add adventure-fall_from_world_height
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-fall_from_world_height-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-fall_from_world_height,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-fall_from_world_height

execute if entity @a[limit=1,team=red,advancements={adventure/avoid_vibration=true}] run advancement grant @a[team=red] only minecraft:adventure/avoid_vibration
execute if entity @a[limit=1,team=red,advancements={adventure/avoid_vibration=true}] run tag @e[type=armor_stand,tag=!adventure-avoid_vibration-ok,name="red"] add adventure-avoid_vibration
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-avoid_vibration-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-avoid_vibration,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-avoid_vibration

execute if entity @a[limit=1,team=red,advancements={adventure/sleep_in_bed=true}] run advancement grant @a[team=red] only minecraft:adventure/sleep_in_bed
execute if entity @a[limit=1,team=red,advancements={adventure/sleep_in_bed=true}] run tag @e[type=armor_stand,tag=!adventure-sleep_in_bed-ok,name="red"] add adventure-sleep_in_bed
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-sleep_in_bed-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sleep_in_bed,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-sleep_in_bed

execute if entity @a[limit=1,team=red,advancements={adventure/hero_of_the_village=true}] run advancement grant @a[team=red] only minecraft:adventure/hero_of_the_village
execute if entity @a[limit=1,team=red,advancements={adventure/hero_of_the_village=true}] run tag @e[type=armor_stand,tag=!adventure-hero_of_the_village-ok,name="red"] add adventure-hero_of_the_village
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-hero_of_the_village-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-hero_of_the_village,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-hero_of_the_village

execute if entity @a[limit=1,team=red,advancements={adventure/spyglass_at_ghast=true}] run advancement grant @a[team=red] only minecraft:adventure/spyglass_at_ghast
execute if entity @a[limit=1,team=red,advancements={adventure/spyglass_at_ghast=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_ghast-ok,name="red"] add adventure-spyglass_at_ghast
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-spyglass_at_ghast-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_ghast,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-spyglass_at_ghast

execute if entity @a[limit=1,team=red,advancements={adventure/throw_trident=true}] run advancement grant @a[team=red] only minecraft:adventure/throw_trident
execute if entity @a[limit=1,team=red,advancements={adventure/throw_trident=true}] run tag @e[type=armor_stand,tag=!adventure-throw_trident-ok,name="red"] add adventure-throw_trident
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-throw_trident-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-throw_trident,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-throw_trident

execute if entity @a[limit=1,team=red,advancements={adventure/kill_mob_near_sculk_catalyst=true}] run advancement grant @a[team=red] only minecraft:adventure/kill_mob_near_sculk_catalyst
execute if entity @a[limit=1,team=red,advancements={adventure/kill_mob_near_sculk_catalyst=true}] run tag @e[type=armor_stand,tag=!adventure-kill_mob_near_sculk_catalyst-ok,name="red"] add adventure-kill_mob_near_sculk_catalyst
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-kill_mob_near_sculk_catalyst-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_mob_near_sculk_catalyst,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-kill_mob_near_sculk_catalyst

execute if entity @a[limit=1,team=red,advancements={adventure/shoot_arrow=true}] run advancement grant @a[team=red] only minecraft:adventure/shoot_arrow
execute if entity @a[limit=1,team=red,advancements={adventure/shoot_arrow=true}] run tag @e[type=armor_stand,tag=!adventure-shoot_arrow-ok,name="red"] add adventure-shoot_arrow
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-shoot_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-shoot_arrow,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-shoot_arrow

execute if entity @a[limit=1,team=red,advancements={adventure/kill_all_mobs=true}] run advancement grant @a[team=red] only minecraft:adventure/kill_all_mobs
execute if entity @a[limit=1,team=red,advancements={adventure/kill_all_mobs=true}] run tag @e[type=armor_stand,tag=!adventure-kill_all_mobs-ok,name="red"] add adventure-kill_all_mobs
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-kill_all_mobs-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-kill_all_mobs,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-kill_all_mobs

execute if entity @a[limit=1,team=red,advancements={adventure/totem_of_undying=true}] run advancement grant @a[team=red] only minecraft:adventure/totem_of_undying
execute if entity @a[limit=1,team=red,advancements={adventure/totem_of_undying=true}] run tag @e[type=armor_stand,tag=!adventure-totem_of_undying-ok,name="red"] add adventure-totem_of_undying
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-totem_of_undying-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-totem_of_undying,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-totem_of_undying

execute if entity @a[limit=1,team=red,advancements={adventure/summon_iron_golem=true}] run advancement grant @a[team=red] only minecraft:adventure/summon_iron_golem
execute if entity @a[limit=1,team=red,advancements={adventure/summon_iron_golem=true}] run tag @e[type=armor_stand,tag=!adventure-summon_iron_golem-ok,name="red"] add adventure-summon_iron_golem
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-summon_iron_golem-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-summon_iron_golem,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-summon_iron_golem

execute if entity @a[limit=1,team=red,advancements={adventure/trade_at_world_height=true}] run advancement grant @a[team=red] only minecraft:adventure/trade_at_world_height
execute if entity @a[limit=1,team=red,advancements={adventure/trade_at_world_height=true}] run tag @e[type=armor_stand,tag=!adventure-trade_at_world_height-ok,name="red"] add adventure-trade_at_world_height
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-trade_at_world_height-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-trade_at_world_height,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-trade_at_world_height

execute if entity @a[limit=1,team=red,advancements={adventure/two_birds_one_arrow=true}] run advancement grant @a[team=red] only minecraft:adventure/two_birds_one_arrow
execute if entity @a[limit=1,team=red,advancements={adventure/two_birds_one_arrow=true}] run tag @e[type=armor_stand,tag=!adventure-two_birds_one_arrow-ok,name="red"] add adventure-two_birds_one_arrow
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-two_birds_one_arrow-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-two_birds_one_arrow,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-two_birds_one_arrow

execute if entity @a[limit=1,team=red,advancements={adventure/whos_the_pillager_now=true}] run advancement grant @a[team=red] only minecraft:adventure/whos_the_pillager_now
execute if entity @a[limit=1,team=red,advancements={adventure/whos_the_pillager_now=true}] run tag @e[type=armor_stand,tag=!adventure-whos_the_pillager_now-ok,name="red"] add adventure-whos_the_pillager_now
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-whos_the_pillager_now-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-whos_the_pillager_now,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-whos_the_pillager_now

execute if entity @a[limit=1,team=red,advancements={adventure/arbalistic=true}] run advancement grant @a[team=red] only minecraft:adventure/arbalistic
execute if entity @a[limit=1,team=red,advancements={adventure/arbalistic=true}] run tag @e[type=armor_stand,tag=!adventure-arbalistic-ok,name="red"] add adventure-arbalistic
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-arbalistic-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-arbalistic,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-arbalistic

execute if entity @a[limit=1,team=red,advancements={adventure/adventuring_time=true}] run advancement grant @a[team=red] only minecraft:adventure/adventuring_time
execute if entity @a[limit=1,team=red,advancements={adventure/adventuring_time=true}] run tag @e[type=armor_stand,tag=!adventure-adventuring_time-ok,name="red"] add adventure-adventuring_time
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-adventuring_time-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-adventuring_time,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-adventuring_time

execute if entity @a[limit=1,team=red,advancements={adventure/play_jukebox_in_meadows=true}] run advancement grant @a[team=red] only minecraft:adventure/play_jukebox_in_meadows
execute if entity @a[limit=1,team=red,advancements={adventure/play_jukebox_in_meadows=true}] run tag @e[type=armor_stand,tag=!adventure-play_jukebox_in_meadows-ok,name="red"] add adventure-play_jukebox_in_meadows
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-play_jukebox_in_meadows-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-play_jukebox_in_meadows,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-play_jukebox_in_meadows

execute if entity @a[limit=1,team=red,advancements={adventure/walk_on_powder_snow_with_leather_boots=true}] run advancement grant @a[team=red] only minecraft:adventure/walk_on_powder_snow_with_leather_boots
execute if entity @a[limit=1,team=red,advancements={adventure/walk_on_powder_snow_with_leather_boots=true}] run tag @e[type=armor_stand,tag=!adventure-walk_on_powder_snow_with_leather_boots-ok,name="red"] add adventure-walk_on_powder_snow_with_leather_boots
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-walk_on_powder_snow_with_leather_boots-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-walk_on_powder_snow_with_leather_boots,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-walk_on_powder_snow_with_leather_boots

execute if entity @a[limit=1,team=red,advancements={adventure/spyglass_at_dragon=true}] run advancement grant @a[team=red] only minecraft:adventure/spyglass_at_dragon
execute if entity @a[limit=1,team=red,advancements={adventure/spyglass_at_dragon=true}] run tag @e[type=armor_stand,tag=!adventure-spyglass_at_dragon-ok,name="red"] add adventure-spyglass_at_dragon
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-spyglass_at_dragon-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-spyglass_at_dragon,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-spyglass_at_dragon

execute if entity @a[limit=1,team=red,advancements={adventure/very_very_frightening=true}] run advancement grant @a[team=red] only minecraft:adventure/very_very_frightening
execute if entity @a[limit=1,team=red,advancements={adventure/very_very_frightening=true}] run tag @e[type=armor_stand,tag=!adventure-very_very_frightening-ok,name="red"] add adventure-very_very_frightening
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-very_very_frightening-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-very_very_frightening,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-very_very_frightening

execute if entity @a[limit=1,team=red,advancements={adventure/sniper_duel=true}] run advancement grant @a[team=red] only minecraft:adventure/sniper_duel
execute if entity @a[limit=1,team=red,advancements={adventure/sniper_duel=true}] run tag @e[type=armor_stand,tag=!adventure-sniper_duel-ok,name="red"] add adventure-sniper_duel
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-sniper_duel-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-sniper_duel,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-sniper_duel

execute if entity @a[limit=1,team=red,advancements={adventure/bullseye=true}] run advancement grant @a[team=red] only minecraft:adventure/bullseye
execute if entity @a[limit=1,team=red,advancements={adventure/bullseye=true}] run tag @e[type=armor_stand,tag=!adventure-bullseye-ok,name="red"] add adventure-bullseye
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="red"] run tag @e[type=armor_stand,name="red"] add adventure-bullseye-ok
execute if entity @e[type=armor_stand,limit=1,tag=adventure-bullseye,name="red"] run tag @e[type=armor_stand,name="red"] remove adventure-bullseye

execute if entity @a[limit=1,team=red,advancements={husbandry/root=true}] run advancement grant @a[team=red] only minecraft:husbandry/root
execute if entity @a[limit=1,team=red,advancements={husbandry/root=true}] run tag @e[type=armor_stand,tag=!husbandry-root-ok,name="red"] add husbandry-root
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-root-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-root,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-root

execute if entity @a[limit=1,team=red,advancements={husbandry/safely_harvest_honey=true}] run advancement grant @a[team=red] only minecraft:husbandry/safely_harvest_honey
execute if entity @a[limit=1,team=red,advancements={husbandry/safely_harvest_honey=true}] run tag @e[type=armor_stand,tag=!husbandry-safely_harvest_honey-ok,name="red"] add husbandry-safely_harvest_honey
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-safely_harvest_honey-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-safely_harvest_honey,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-safely_harvest_honey

execute if entity @a[limit=1,team=red,advancements={husbandry/breed_an_animal=true}] run advancement grant @a[team=red] only minecraft:husbandry/breed_an_animal
execute if entity @a[limit=1,team=red,advancements={husbandry/breed_an_animal=true}] run tag @e[type=armor_stand,tag=!husbandry-breed_an_animal-ok,name="red"] add husbandry-breed_an_animal
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-breed_an_animal-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-breed_an_animal,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-breed_an_animal

execute if entity @a[limit=1,team=red,advancements={husbandry/allay_deliver_item_to_player=true}] run advancement grant @a[team=red] only minecraft:husbandry/allay_deliver_item_to_player
execute if entity @a[limit=1,team=red,advancements={husbandry/allay_deliver_item_to_player=true}] run tag @e[type=armor_stand,tag=!husbandry-allay_deliver_item_to_player-ok,name="red"] add husbandry-allay_deliver_item_to_player
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-allay_deliver_item_to_player-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_item_to_player,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-allay_deliver_item_to_player

execute if entity @a[limit=1,team=red,advancements={husbandry/ride_a_boat_with_a_goat=true}] run advancement grant @a[team=red] only minecraft:husbandry/ride_a_boat_with_a_goat
execute if entity @a[limit=1,team=red,advancements={husbandry/ride_a_boat_with_a_goat=true}] run tag @e[type=armor_stand,tag=!husbandry-ride_a_boat_with_a_goat-ok,name="red"] add husbandry-ride_a_boat_with_a_goat
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-ride_a_boat_with_a_goat-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-ride_a_boat_with_a_goat,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-ride_a_boat_with_a_goat

execute if entity @a[limit=1,team=red,advancements={husbandry/tame_an_animal=true}] run advancement grant @a[team=red] only minecraft:husbandry/tame_an_animal
execute if entity @a[limit=1,team=red,advancements={husbandry/tame_an_animal=true}] run tag @e[type=armor_stand,tag=!husbandry-tame_an_animal-ok,name="red"] add husbandry-tame_an_animal
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-tame_an_animal-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tame_an_animal,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-tame_an_animal

execute if entity @a[limit=1,team=red,advancements={husbandry/make_a_sign_glow=true}] run advancement grant @a[team=red] only minecraft:husbandry/make_a_sign_glow
execute if entity @a[limit=1,team=red,advancements={husbandry/make_a_sign_glow=true}] run tag @e[type=armor_stand,tag=!husbandry-make_a_sign_glow-ok,name="red"] add husbandry-make_a_sign_glow
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-make_a_sign_glow-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-make_a_sign_glow,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-make_a_sign_glow

execute if entity @a[limit=1,team=red,advancements={husbandry/fishy_business=true}] run advancement grant @a[team=red] only minecraft:husbandry/fishy_business
execute if entity @a[limit=1,team=red,advancements={husbandry/fishy_business=true}] run tag @e[type=armor_stand,tag=!husbandry-fishy_business-ok,name="red"] add husbandry-fishy_business
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-fishy_business-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-fishy_business,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-fishy_business

execute if entity @a[limit=1,team=red,advancements={husbandry/silk_touch_nest=true}] run advancement grant @a[team=red] only minecraft:husbandry/silk_touch_nest
execute if entity @a[limit=1,team=red,advancements={husbandry/silk_touch_nest=true}] run tag @e[type=armor_stand,tag=!husbandry-silk_touch_nest-ok,name="red"] add husbandry-silk_touch_nest
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-silk_touch_nest-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-silk_touch_nest,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-silk_touch_nest

execute if entity @a[limit=1,team=red,advancements={husbandry/tadpole_in_a_bucket=true}] run advancement grant @a[team=red] only minecraft:husbandry/tadpole_in_a_bucket
execute if entity @a[limit=1,team=red,advancements={husbandry/tadpole_in_a_bucket=true}] run tag @e[type=armor_stand,tag=!husbandry-tadpole_in_a_bucket-ok,name="red"] add husbandry-tadpole_in_a_bucket
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-tadpole_in_a_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tadpole_in_a_bucket,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-tadpole_in_a_bucket

execute if entity @a[limit=1,team=red,advancements={husbandry/plant_seed=true}] run advancement grant @a[team=red] only minecraft:husbandry/plant_seed
execute if entity @a[limit=1,team=red,advancements={husbandry/plant_seed=true}] run tag @e[type=armor_stand,tag=!husbandry-plant_seed-ok,name="red"] add husbandry-plant_seed
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-plant_seed-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-plant_seed,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-plant_seed

execute if entity @a[limit=1,team=red,advancements={husbandry/wax_on=true}] run advancement grant @a[team=red] only minecraft:husbandry/wax_on
execute if entity @a[limit=1,team=red,advancements={husbandry/wax_on=true}] run tag @e[type=armor_stand,tag=!husbandry-wax_on-ok,name="red"] add husbandry-wax_on
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-wax_on-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_on,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-wax_on

execute if entity @a[limit=1,team=red,advancements={husbandry/bred_all_animals=true}] run advancement grant @a[team=red] only minecraft:husbandry/bred_all_animals
execute if entity @a[limit=1,team=red,advancements={husbandry/bred_all_animals=true}] run tag @e[type=armor_stand,tag=!husbandry-bred_all_animals-ok,name="red"] add husbandry-bred_all_animals
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-bred_all_animals-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-bred_all_animals,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-bred_all_animals

execute if entity @a[limit=1,team=red,advancements={husbandry/allay_deliver_cake_to_note_block=true}] run advancement grant @a[team=red] only minecraft:husbandry/allay_deliver_cake_to_note_block
execute if entity @a[limit=1,team=red,advancements={husbandry/allay_deliver_cake_to_note_block=true}] run tag @e[type=armor_stand,tag=!husbandry-allay_deliver_cake_to_note_block-ok,name="red"] add husbandry-allay_deliver_cake_to_note_block
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-allay_deliver_cake_to_note_block-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-allay_deliver_cake_to_note_block,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-allay_deliver_cake_to_note_block

execute if entity @a[limit=1,team=red,advancements={husbandry/complete_catalogue=true}] run advancement grant @a[team=red] only minecraft:husbandry/complete_catalogue
execute if entity @a[limit=1,team=red,advancements={husbandry/complete_catalogue=true}] run tag @e[type=armor_stand,tag=!husbandry-complete_catalogue-ok,name="red"] add husbandry-complete_catalogue
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-complete_catalogue-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-complete_catalogue,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-complete_catalogue

execute if entity @a[limit=1,team=red,advancements={husbandry/tactical_fishing=true}] run advancement grant @a[team=red] only minecraft:husbandry/tactical_fishing
execute if entity @a[limit=1,team=red,advancements={husbandry/tactical_fishing=true}] run tag @e[type=armor_stand,tag=!husbandry-tactical_fishing-ok,name="red"] add husbandry-tactical_fishing
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-tactical_fishing-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-tactical_fishing,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-tactical_fishing

execute if entity @a[limit=1,team=red,advancements={husbandry/leash_all_frog_variants=true}] run advancement grant @a[team=red] only minecraft:husbandry/leash_all_frog_variants
execute if entity @a[limit=1,team=red,advancements={husbandry/leash_all_frog_variants=true}] run tag @e[type=armor_stand,tag=!husbandry-leash_all_frog_variants-ok,name="red"] add husbandry-leash_all_frog_variants
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-leash_all_frog_variants-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-leash_all_frog_variants,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-leash_all_frog_variants

execute if entity @a[limit=1,team=red,advancements={husbandry/balanced_diet=true}] run advancement grant @a[team=red] only minecraft:husbandry/balanced_diet
execute if entity @a[limit=1,team=red,advancements={husbandry/balanced_diet=true}] run tag @e[type=armor_stand,tag=!husbandry-balanced_diet-ok,name="red"] add husbandry-balanced_diet
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-balanced_diet-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-balanced_diet,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-balanced_diet

execute if entity @a[limit=1,team=red,advancements={husbandry/obtain_netherite_hoe=true}] run advancement grant @a[team=red] only minecraft:husbandry/obtain_netherite_hoe
execute if entity @a[limit=1,team=red,advancements={husbandry/obtain_netherite_hoe=true}] run tag @e[type=armor_stand,tag=!husbandry-obtain_netherite_hoe-ok,name="red"] add husbandry-obtain_netherite_hoe
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-obtain_netherite_hoe-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-obtain_netherite_hoe,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-obtain_netherite_hoe

execute if entity @a[limit=1,team=red,advancements={husbandry/wax_off=true}] run advancement grant @a[team=red] only minecraft:husbandry/wax_off
execute if entity @a[limit=1,team=red,advancements={husbandry/wax_off=true}] run tag @e[type=armor_stand,tag=!husbandry-wax_off-ok,name="red"] add husbandry-wax_off
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-wax_off-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-wax_off,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-wax_off

execute if entity @a[limit=1,team=red,advancements={husbandry/axolotl_in_a_bucket=true}] run advancement grant @a[team=red] only minecraft:husbandry/axolotl_in_a_bucket
execute if entity @a[limit=1,team=red,advancements={husbandry/axolotl_in_a_bucket=true}] run tag @e[type=armor_stand,tag=!husbandry-axolotl_in_a_bucket-ok,name="red"] add husbandry-axolotl_in_a_bucket
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-axolotl_in_a_bucket-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-axolotl_in_a_bucket,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-axolotl_in_a_bucket

execute if entity @a[limit=1,team=red,advancements={husbandry/froglights=true}] run advancement grant @a[team=red] only minecraft:husbandry/froglights
execute if entity @a[limit=1,team=red,advancements={husbandry/froglights=true}] run tag @e[type=armor_stand,tag=!husbandry-froglights-ok,name="red"] add husbandry-froglights
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-froglights-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-froglights,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-froglights

execute if entity @a[limit=1,team=red,advancements={husbandry/kill_axolotl_target=true}] run advancement grant @a[team=red] only minecraft:husbandry/kill_axolotl_target
execute if entity @a[limit=1,team=red,advancements={husbandry/kill_axolotl_target=true}] run tag @e[type=armor_stand,tag=!husbandry-kill_axolotl_target-ok,name="red"] add husbandry-kill_axolotl_target
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="red"] run scoreboard players add red Scores 1
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="red"] run tag @e[type=armor_stand,name="red"] add husbandry-kill_axolotl_target-ok
execute if entity @e[type=armor_stand,limit=1,tag=husbandry-kill_axolotl_target,name="red"] run tag @e[type=armor_stand,name="red"] remove husbandry-kill_axolotl_target

schedule function bingo:red 1s