team add red
team add blue
team add green
team add yellow
team add black
team add white
team add aqua
team add gray
team add gold
team add dark_purple
team add dark_blue
team add dark_green
team add dark_aqua
team add dark_red
team add dark_gray
team add light_purple

team modify red color red
team modify blue color blue
team modify green color green
team modify yellow color yellow
team modify black color black
team modify white color white
team modify aqua color aqua
team modify gray color gray
team modify gold color gold
team modify dark_purple color dark_purple
team modify dark_blue color dark_blue
team modify dark_green color dark_green
team modify dark_aqua color dark_aqua
team modify dark_red color dark_red
team modify dark_gray color dark_gray
team modify light_purple color light_purple

team join red red
team join blue blue
team join green green
team join yellow yellow
team join black black
team join white white
team join aqua aqua
team join gray gray
team join gold gold
team join dark_purple dark_purple
team join dark_blue dark_blue
team join dark_green dark_green
team join dark_aqua dark_aqua
team join dark_red dark_red
team join dark_gray dark_gray
team join light_purple light_purple

schedule function bingo:red 1s
schedule function bingo:blue 1s
schedule function bingo:green 1s
schedule function bingo:yellow 1s
schedule function bingo:black 1s
schedule function bingo:white 1s
schedule function bingo:aqua 1s
schedule function bingo:gray 1s
schedule function bingo:gold 1s
schedule function bingo:dark_purple 1s
schedule function bingo:dark_blue 1s
schedule function bingo:dark_green 1s
schedule function bingo:dark_aqua 1s
schedule function bingo:dark_red 1s
schedule function bingo:dark_gray 1s
schedule function bingo:light_purple 1s

summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"red"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"blue"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"green"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"yellow"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"black"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"white"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"aqua"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"gray"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"gold"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"dark_purple"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"dark_blue"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"dark_green"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"dark_aqua"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"dark_red"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"dark_gray"}'}
summon armor_stand 0 200 0 {NoGravity:1b,Invulnerable:1b,Invisible:1b,CustomName:'{"text":"light_purple"}'}

scoreboard objectives add Scores dummy
scoreboard objectives add scoretmp dummy
scoreboard objectives setdisplay sidebar Scores