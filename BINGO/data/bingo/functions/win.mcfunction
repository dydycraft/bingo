scoreboard players operation @e[type=armor_stand,limit=1,name="red"] scoretmp = red Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="blue"] scoretmp = blue Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="green"] scoretmp = green Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="yellow"] scoretmp = yellow Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="black"] scoretmp = black Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="white"] scoretmp = white Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="aqua"] scoretmp = aqua Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="gray"] scoretmp = gray Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="gold"] scoretmp = gold Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="dark_purple"] scoretmp = dark_purple Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="dark_blue"] scoretmp = dark_blue Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="dark_green"] scoretmp = dark_green Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="dark_aqua"] scoretmp = dark_aqua Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="dark_red"] scoretmp = dark_red Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="dark_gray"] scoretmp = dark_gray Scores
scoreboard players operation @e[type=armor_stand,limit=1,name="light_purple"] scoretmp = light_purple Scores

execute if entity @e[type=armor_stand,limit=1,name="red",scores={scoretmp=102..}] run title @a title {"text":"Red Win","color":"red"}
execute if entity @e[type=armor_stand,limit=1,name="blue",scores={scoretmp=102..}] run title @a title {"text":"Blue Win","color":"blue"}
execute if entity @e[type=armor_stand,limit=1,name="green",scores={scoretmp=102..}] run title @a title {"text":"Green Win","color":"green"}
execute if entity @e[type=armor_stand,limit=1,name="yellow",scores={scoretmp=102..}] run title @a title {"text":"Yellow Win","color":"yellow"}
execute if entity @e[type=armor_stand,limit=1,name="black",scores={scoretmp=102..}] run title @a title {"text":"Black Win","color":"black"}
execute if entity @e[type=armor_stand,limit=1,name="white",scores={scoretmp=102..}] run title @a title {"text":"White Win","color":"white"}
execute if entity @e[type=armor_stand,limit=1,name="aqua",scores={scoretmp=102..}] run title @a title {"text":"Aqua Win","color":"aqua"}
execute if entity @e[type=armor_stand,limit=1,name="gray",scores={scoretmp=102..}] run title @a title {"text":"Gray Win","color":"gray"}
execute if entity @e[type=armor_stand,limit=1,name="gold",scores={scoretmp=102..}] run title @a title {"text":"Gold Win","color":"gold"}
execute if entity @e[type=armor_stand,limit=1,name="dark_purple",scores={scoretmp=102..}] run title @a title {"text":"Dark_purple Win","color":"dark_purple"}
execute if entity @e[type=armor_stand,limit=1,name="dark_blue",scores={scoretmp=102..}] run title @a title {"text":"Dark_blue Win","color":"dark_blue"}
execute if entity @e[type=armor_stand,limit=1,name="dark_green",scores={scoretmp=102..}] run title @a title {"text":"Dark_green Win","color":"dark_green"}
execute if entity @e[type=armor_stand,limit=1,name="dark_aqua",scores={scoretmp=102..}] run title @a title {"text":"Dark_aqua Win","color":"dark_aqua"}
execute if entity @e[type=armor_stand,limit=1,name="dark_red",scores={scoretmp=102..}] run title @a title {"text":"Dark_red Win","color":"dark_red"}
execute if entity @e[type=armor_stand,limit=1,name="dark_gray",scores={scoretmp=102..}] run title @a title {"text":"Dark_gray Win","color":"dark_gray"}
execute if entity @e[type=armor_stand,limit=1,name="light_purple",scores={scoretmp=102..}] run title @a title {"text":"Light_purple Win","color":"light_purple"}

schedule function bingo:win 1s